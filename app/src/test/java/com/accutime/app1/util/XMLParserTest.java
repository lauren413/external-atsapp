package com.accutime.app1.util;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;

/**
 * Created by mkemp on 6/27/18.
 */
public class XMLParserTest {

    public static final String TAG = XMLParserTest.class.getSimpleName();

    private static final String HEARTBEAT_XML =
            "                <Heartbeat localinstanceId=\"74D7A913AE\">\n" +
            "\n" +
            "                   <Version localinstanceId=\"761E6A2652\">\n" +
            "\n" +
            "                       <DataLists localinstanceId=\"5EB98BA236\">\n" +
            "\n" +
            "                           <Version localinstanceId=\"441E0016B4\">4769</Version>\n" +
            "                           <ListName localinstanceId=\"FFA069499E\">JOB</ListName></DataLists>\n" +
            "\n" +
            "                       <Tasks localinstanceId=\"C39962F432\">0</Tasks>\n" +
            "                       <Biometrics localinstanceId=\"91E77B90F9\">17</Biometrics>\n" +
            "                       <AppTables localinstanceId=\"C180E74486\">0</AppTables>\n" +
            "                       <Schedule localinstanceId=\"586BB3323A\">0</Schedule>\n" +
            "                       <Employee localinstanceId=\"ACC52BCE5F\" wtdhours=\"0\" messages=\"0\" core=\"12467\">12467.0.0</Employee>\n" +
            "                       <Flash localinstanceId=\"3CA96135F3\">33</Flash>\n" +
            "                       <Config localinstanceId=\"0D2152C8C8\">11</Config>\n" +
            "                       <Dld localinstanceId=\"E3286AB914\">22</Dld>\n" +
            "                    </Version>\n" +
            "                </Heartbeat>";

    private static final String PUNCHES_XML = "<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
            "        <Punches" +
            "            terminal=\"12345\"" +
            "            tz=\"America/New_York\"" +
            "            uid=\"f81d4fae-7dec-11d0-a765-00a0c91e6bf6\"" +
            "            mode=\"buffered\">" +
            "            <Punch>\n" +
            "                <Badge>123456789</Badge>\n" +
            "                <Timestamp>2009-09-04T14:23:35.000000+0600</Timestamp>\n" +
            "                <Action>IN</Action>\n" +
            "            </Punch>\n" +
            "        </Punches>";

    // Heartbeat
    private static final String HEARTBEAT_KEY = "Heartbeat";
    private static final String VERSION_TAG = "Version";

    private static final String CONFIG_KEY = "Config";
    private static final String CONFIG_VALUE = "11";

    private static final String DLD_KEY = "Dld";
    private static final String DLD_VALUE = "22";

    private static final String FLASH_KEY = "Flash";
    private static final String FLASH_VALUE = "33";

    // Punches
    private static final String PUNCHES_KEY = "Punches";
    private static final String PUNCH_TAG = "Punch";

    private static final String BADGE_KEY = "Badge";
    private static final String BADGE_VALUE = "123456789";

    private static final String TIMESTAMP_KEY = "Timestamp";
    private static final String TIMESTAMP_VALUE = "2009-09-04T14:23:35.000000+0600";

    private static final String ACTION_KEY = "Action";
    private static final String ACTION_VALUE = "IN";

    @Test
    public void getDomElement() throws Exception {
        // Heartbeat //
        Document heartbeatDocument = XMLParser.getDomElement(HEARTBEAT_XML);
        assertNotNull(heartbeatDocument);

        Element heartbeatElement = heartbeatDocument.getDocumentElement();
        assertEquals(HEARTBEAT_KEY, heartbeatElement.getTagName());

        // Punches //
        Document punchesDocument = XMLParser.getDomElement(PUNCHES_XML);
        assertNotNull(punchesDocument);

        Element punchesElement = punchesDocument.getDocumentElement();
        assertEquals(PUNCHES_KEY, punchesElement.getTagName());
    }

    @Test
    public void findValueInXML() throws Exception {
        // Heartbeat //
        Document rootNode = XMLParser.getDomElement(HEARTBEAT_XML);
        if (rootNode != null) {
            String config = XMLParser.findValueInXML(rootNode, VERSION_TAG, CONFIG_KEY);
            String dld = XMLParser.findValueInXML(rootNode, VERSION_TAG, DLD_KEY);
            String flash = XMLParser.findValueInXML(rootNode, VERSION_TAG, FLASH_KEY);

            assertEquals(CONFIG_VALUE, config);
            assertEquals(DLD_VALUE, dld);
            assertEquals(FLASH_VALUE, flash);
        }

        // Punches //
        Document punchesRootNode = XMLParser.getDomElement(PUNCHES_XML);
        if (punchesRootNode != null) {
            String badge = XMLParser.findValueInXML(punchesRootNode, PUNCH_TAG, BADGE_KEY);
            String timestamp = XMLParser.findValueInXML(punchesRootNode, PUNCH_TAG, TIMESTAMP_KEY);
            String action = XMLParser.findValueInXML(punchesRootNode, PUNCH_TAG, ACTION_KEY);

            assertEquals(BADGE_VALUE, badge);
            assertEquals(TIMESTAMP_VALUE, timestamp);
            assertEquals(ACTION_VALUE, action);
        }
    }

    @Test
    public void getValue() throws Exception {
    }

    @Test
    public void getElementValue() throws Exception {
    }

}