package com.accutime.app1.services;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class HeartbeatJobTest {

    private static final String VERSION_1 = "1";
    private static final String VERSION_2 = "2";
    private HeartbeatJob heartbeatJob;


    @Before
    public void initMocks() {
        heartbeatJob = new HeartbeatJob();
    }

    @Test
    public void check_two_different_versions_needs_upgrade() {
        boolean match = heartbeatJob.needsUpdate(VERSION_1, VERSION_2);
        assertEquals(true, match);
    }

    @Test
    public void check_two_same_versions_do_not_need_upgrade() {
        boolean match = heartbeatJob.needsUpdate(VERSION_1, VERSION_1);
        assertEquals(false, match);
    }
}