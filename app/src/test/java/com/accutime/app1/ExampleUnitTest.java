package com.accutime.app1;

import org.junit.Test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static junit.framework.Assert.assertEquals;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }

//    String desiredSystemTime = "15 Nov 1994 08:12:31 GMT";
    String desiredSystemTime = "08 Aug 2017 3:33 GMT-5";
    @Test
    public void date_formats_correctly() throws Exception {

//        Date givenDay;
//        String ouput;
//        SimpleDateFormat formatter;
//
//        formatter = new SimpleDateFormat()

        // Weed out time zone
        String[] dateParts = desiredSystemTime.split(" ");
        String timeZone = dateParts[dateParts.length - 1];
        System.out.println("Time zone should be: " + timeZone);

        // Create parser
        DateFormat df = new SimpleDateFormat("d MMM yyyy H:mm");

        // Change time zone of parser
        df.setTimeZone(TimeZone.getTimeZone(timeZone));

//        System.out.println(df.getTim)

//        String maybeTimeZone = String.valueOf(parser.parse("zzz"));
//        System.out.println("Maybe time zone: " + maybeTimeZone);

//        // Show initial time zone
//        String timezone = String.valueOf(parser.getTimeZone());
//        System.out.println("Starting timezone: " + timezone);
//
//        // Change time zone
//        parser.setTimeZone(TimeZone.getTimeZone("GMT")); // parse defaults to my time zone. need to set it to theirs...
//
//        // Show changed time zone
//        timezone = String.valueOf(parser.getTimeZone());
//        System.out.println("Now timezone: " + timezone);


        Date date = null;
        try {
            // Parse string input to date
//            String target = "28 Sep 2000 20:29:30 JST";
            date = df.parse(desiredSystemTime);

            // Show date
            System.out.println("Date: " + date);

            String parserDate = df.format(date);
            System.out.println("Format the parser: " + parserDate);

//            String afterFormatting = String.valueOf(parser.parse(parserDate));
//            System.out.println("Date is now: " + afterFormatting);
//
//            date = parser.parse(parserDate);
//            System.out.println("Newly created date: " + date);




            int day = Integer.parseInt(new SimpleDateFormat("dd").format(date));
            int month = Integer.parseInt(new SimpleDateFormat("MM").format(date));
            int year = Integer.parseInt(new SimpleDateFormat("yyyy").format(date));
            int hour = Integer.parseInt(new SimpleDateFormat("HH").format(date));
            int minute = Integer.parseInt(new SimpleDateFormat("mm").format(date));
//            String timeZone = new SimpleDateFormat("zzz").format(date);

            // Doesn't matter if this is off...
            // as long as I'm sure to set the correct time zone, it will fix itself.
            System.out.println("Day is " + day);
            System.out.println("Month is " + month);
            System.out.println("Year is " + year);
            System.out.println("Hour is " + hour);
            System.out.println("Minute is " + minute);
            System.out.println("Zone is " + timeZone);
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    @Test
    public void a() throws ParseException {
        SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        isoFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = isoFormat.parse("2010-05-23T09:01:02");
        System.out.println(date);
    }
}