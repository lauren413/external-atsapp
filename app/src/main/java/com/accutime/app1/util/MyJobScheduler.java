package com.accutime.app1.util;

import android.app.job.JobInfo;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.os.PersistableBundle;
import android.support.annotation.RequiresApi;

import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.services.GetEmployeesJob;
import com.accutime.app1.services.HeartbeatJob;
import com.accutime.app1.services.PunchJob;
import com.google.gson.Gson;

import java.util.concurrent.TimeUnit;

/**
 * Created by mkemp on 5/15/18.
 */

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class MyJobScheduler {

    private static final int HEARTBEAT_JOB_ID = 1246;
    private static final int GET_EMPLOYEES_JOB_ID = 3476;
    private static final int PUNCH_JOB_ID = 4386;

    private static final int HEARTBEAT_PERIOD = (int) TimeUnit.SECONDS.toMillis(60);

    private static final int SEND_PUNCH_BATCH_PERIOD = (int) TimeUnit.SECONDS.toMillis(15);

    /**
     * Schedule job to get heartbeat
     */
    public static void scheduleHeartbeatJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, HeartbeatJob.class);

        JobInfo.Builder builder = new JobInfo.Builder(HEARTBEAT_JOB_ID, serviceComponent);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setPeriodic(HEARTBEAT_PERIOD);
        builder.setPersisted(true);

        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.cancel(HEARTBEAT_JOB_ID);
        if (jobScheduler != null) { jobScheduler.schedule(builder.build()); }
    }

    /**
     * Schedule job to get employees
     */
    public static void scheduleGetEmployeesJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, GetEmployeesJob.class);

        JobInfo.Builder builder = new JobInfo.Builder(GET_EMPLOYEES_JOB_ID, serviceComponent);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setPersisted(true);

        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.cancel(GET_EMPLOYEES_JOB_ID);
        if (jobScheduler != null) { jobScheduler.schedule(builder.build()); }
    }

    /**
     * Schedule job to send punch
     */
    // TODO : This only need to accept a batch, which is then pushed. Is punches a batch...?
    // Nope. Punches is just a class. I could make a list of Punches...
//    public static void schedulePunchJob(Context context, AtsService.Punches punches, String message) {
//
//        AtsService.Punches batch = createNewBatch();
//
//        // I got that punch. I have an existing punch list...
//        // Now add this puch to the list
//        batch.punchList.add(punch);
//
//        ComponentName serviceComponent = new ComponentName(context, PunchJob.class);
//
//        // TODO : Make this periodic
//        JobInfo.Builder builder = new JobInfo.Builder(PUNCH_JOB_ID, serviceComponent);
//        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
//        builder.setPeriodic(SEND_PUNCH_BATCH_PERIOD);
//        builder.setPersisted(true);
////        builder.setMinimumLatency(500);
////        builder.setPersisted(true);
//
//        PersistableBundle bundle = getPunchBundle(punches, message);
//        builder.setExtras(bundle);
//
//        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
////        jobScheduler.cancel(PUNCH_JOB_ID);
//        if (jobScheduler != null) { jobScheduler.schedule(builder.build()); }
//    }

    /**
     * Helper method to get bundle to send to PunchJob
     */
    private static PersistableBundle getPunchBundle(AtsService.Punches punches, String message) {
        Gson gson = new Gson();

        String json = gson.toJson(punches);
        String messageToUser = gson.toJson(message);

        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("Punches", json);
        bundle.putString("MessageToUser", messageToUser);
        return bundle;
    }


    public static void schedulePunchJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, PunchJob.class);

        JobInfo.Builder builder = new JobInfo.Builder(PUNCH_JOB_ID, serviceComponent);
        builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY);
        builder.setPeriodic(SEND_PUNCH_BATCH_PERIOD);
        builder.setPersisted(true);

        android.app.job.JobScheduler jobScheduler = (android.app.job.JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
//        jobScheduler.cancel(PUNCH_JOB_ID);
        if (jobScheduler != null) { jobScheduler.schedule(builder.build()); }
    }
}
