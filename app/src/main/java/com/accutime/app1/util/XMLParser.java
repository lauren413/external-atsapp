package com.accutime.app1.util;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by mkemp on 6/26/18.
 */

public class XMLParser {

    private static final String TAG = XMLParser.class.getSimpleName();

    public static Document getDomElement(String xml)
    {
        Document document = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {

            DocumentBuilder builder = factory.newDocumentBuilder();

            InputSource inputSource = new InputSource();
            inputSource.setCharacterStream(new StringReader(xml));

            document = builder.parse(inputSource);

        } catch (ParserConfigurationException | SAXException | IOException e) {
            Log.e(TAG, "Error getting dom element from " + xml + ":\n" + e.getMessage());
            return null;
        }

        // Also normalize this
        document.getDocumentElement().normalize();

        // return DOM
        return document;
    }

    /**
     * Given the XML document with an inner element containing multiple children,
     * find the text value from the given node.
     * @param rootNode root xml node
     * @param innerParentName name of the parent node within root that contains desired data
     * @param desiredNode name of the exact grandparent node with desired data
     */
    public static String findValueInXML(Document rootNode, String innerParentName, String desiredNode) {

        // Get all nodes within root node with the desired element name
        // Ex. Version or Punch
        NodeList nodeList = rootNode.getElementsByTagName(innerParentName);
        for (int i = 0; i < nodeList.getLength(); i++) {

            // This node
            // Ex. Version[0] or Punch[0]
            Node node = nodeList.item(i);
//            System.out.println(node.getNodeName());



//            Document document = node.getOwnerDocument();
//            DOMImplementationLS domImplLS = (DOMImplementationLS) document
//                    .getImplementation();
//            LSSerializer serializer = domImplLS.createLSSerializer();
//            String str = serializer.writeToString(node);
//            System.out.println(str);



            // Every XML element is an element node
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                // Cast node to element
                Element element = (Element) node;

                // Get element attribute
                // Ex. value of localinstanceId
//                String attribute = element.getAttribute(LOCALINSTANCE_ID_ATTRIBUTE);

                // Get all children of this node, regardless of name
                NodeList elementChildren = element.getChildNodes();
                for (int j = 0; j < elementChildren.getLength(); j++) {

                    // This child node
                    // Ex. Config, Dld, or Flash < />
                    Node childNode = elementChildren.item(j);

//                    System.out.println("\t"+childNode.getNodeName());

                    // Every XML element is an element node
                    if (childNode.getNodeType() == Node.ELEMENT_NODE) {

                        // Cast node to element
                        Element itemElement = (Element) childNode;

                        // The name of the node
                        String childName = itemElement.getTagName();
//                        System.out.println("\t\t" + childName);
//                        System.out.println("\t\t" + desiredNode);

                        if (childName.equals(desiredNode)) {
                            // Text the node contains
                            String text = childNode.getTextContent();
//                            System.out.println(childName + " = " + text);
                            return text;
                        }
                    }
                }
            }
        }

        return "";
    }

    /**
     * Gets the value of a particular node from the given parent element,
     * assuming that element only has one direct child.
     * @param parent parent element
     * @param tag tag of child element
     * @return desired text
     */
    public String getValueFromElement(Element parent, String tag) {

        NodeList nodeList = parent.getElementsByTagName(tag);
        return getElementValue(nodeList.item(0));
    }

    public final String getElementValue(Node element)
    {
        // This element has children
        if (element != null && element.hasChildNodes()) {

            Node child;

            // Look through them all
            for (child = element.getFirstChild(); child != null; child = child.getNextSibling()) {

                // Child is text - not element -  return it
                if (child.getNodeType() == Node.TEXT_NODE) {
                    return child.getNodeValue();
                }
            }
        }
        return "";
    }
}
