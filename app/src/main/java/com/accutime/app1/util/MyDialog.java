package com.accutime.app1.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Scroller;
import android.widget.TextView;

import com.accutime.app1.R;

import java.util.Objects;

/**
 * Created by mkemp on 5/8/18.
 */

public class MyDialog {

    private static AlertDialog simpleDialog;

    public static void show(Context context, String title, String message, Integer delay)
    {
        // Dismiss any existing dialog
        dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
//        builder.setCancelable(false);
        simpleDialog = builder.create();

        // Show new dialog
        simpleDialog.show();
        setLargeTextSize(context, simpleDialog);

        // Dismiss dialog after delay
        if (delay != null) {
            new Handler().postDelayed(new Runnable() {
                public void run()
                {
                    dismiss();
                }
            }, delay);
        }
    }

    public static void show(Context context, String message, Integer delay)
    {
        // Dismiss any existing dialog
        dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
//        builder.setCancelable(false);
        simpleDialog = builder.create();

        // Show new dialog
        simpleDialog.show();
        setLargeTextSize(context, simpleDialog);

        // Dismiss dialog after delay
        if (delay != null) {
            new Handler().postDelayed(new Runnable() {
                public void run()
                {
                    dismiss();
                }
            }, delay);
        }
    }

    public static void show(Context context, String title, String message)
    {
        // Dismiss any existing dialog
        dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
//        builder.setCancelable(false);
        simpleDialog = builder.create();

        try {
            // Show new dialog
            simpleDialog.show();
            setLargeTextSize(context, simpleDialog);
        } catch (Exception e) {
            Log.e("MyDialog", "Failed to show dialog!\n" + e);
        }
    }

    public static void show(Context context, String message)
    {
        // Dismiss any existing dialog
        dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(message);
//        builder.setCancelable(false);
        simpleDialog = builder.create();

        // Show new dialog
        simpleDialog.show();
        setLargeTextSize(context, simpleDialog);

    }

    public static void show(Context context, String title, String message, DialogInterface.OnClickListener onClickListener)
    {
        // Dismiss any existing dialog
        dismiss();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setCancelable(false);
        builder.setPositiveButton(context.getString(R.string.ok), onClickListener);
        simpleDialog = builder.create();

        Objects.requireNonNull(simpleDialog.getWindow()).setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        // Show new dialog
        simpleDialog.show();
        setLargeTextSize(context, simpleDialog);

    }

    public static void dismiss() {
        if (simpleDialog != null && simpleDialog.isShowing()) {
            simpleDialog.dismiss();
        }
    }

    public static void setLargeTextSize(Context context, AlertDialog simpleDialog) {
        TextView textView = (TextView) simpleDialog.findViewById(android.R.id.message);
        textView.setTextSize(context.getResources().getDimension(R.dimen.large_text));
        textView.setScroller(new Scroller(context));
        textView.setVerticalScrollBarEnabled(true);
        textView.setMovementMethod(new ScrollingMovementMethod());
    }
}
