package com.accutime.app1.util;

import android.app.Activity;
import android.content.Context;
import android.os.CountDownTimer;

import com.accutime.app1.activities.BaseActivity;
import com.accutime.app1.activities.StateActivity;

/**
 * Created by mkemp on 2/16/18.
 */

public class MyCountdownTimer {

    private final String TAG = this.getClass().getSimpleName();
    private Activity activity;
    private CountDownTimer countDownTimer;
    private static final long SHOW_COUNTDOWN_TIME = 4000;

    public MyCountdownTimer(Context context) {
        this.activity = (Activity) context;
    }

    public void start(long duration) {
        countDownTimer = new CountDownTimer(duration, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

//                if (activity instanceof PunchActivity) {
//                    updateTimer((PunchActivity) activity, millisUntilFinished);
//                }

//                else

                if (activity instanceof StateActivity) {
                    updateTimer((StateActivity) activity, millisUntilFinished);
                }

                // TODO Add more activities
            }

            @Override
            public void onFinish() {
                activity.onBackPressed();
                if (activity instanceof BaseActivity) {
                    MyDialog.dismiss();
                }
            }
        };
        countDownTimer.start();
    }

//    private void updateTimer(PunchActivity activity, long millisUntilFinished) {
//        if (millisUntilFinished <= 6000) {
//            String time = String.valueOf((int) (Math.ceil(millisUntilFinished / 1000)));
//            activity.updateTimer(time);
//            activity.showTimer();
//        } else {
//            activity.hideTimer();
//        }
//    }

    private void updateTimer(StateActivity stateActivity, long millisUntilFinished) {
//        if (millisUntilFinished <= SHOW_COUNTDOWN_TIME) {
//            String time = String.valueOf((int) (Math.ceil(millisUntilFinished / 1000)));
//            stateActivity.updateTimer(time);
//            stateActivity.showTimer();
//        } else {
//            stateActivity.hideTimer();
//        }
    }

//    private void updateTimer(PunchActivity punchActivity, long millisUntilFinished) {
//        if (millisUntilFinished <= SHOW_COUNTDOWN_TIME) {
//            String time = String.valueOf((int) (Math.ceil(millisUntilFinished / 1000)));
//            punchActivity.updateTimer(time);
//            punchActivity.showTimer();
//        } else {
//            punchActivity.hideTimer();
//        }
//    }

    public void cancel() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }
}
