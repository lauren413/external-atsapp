package com.accutime.app1.retrofit;

import com.accutime.app1.xmlparser.TreeNode;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Retrofit;

/**
 * A {@linkplain Converter.Factory converter} which uses the built-in Xml parser Framework.
 * <p>
 * This converter only applies for class types. Parameterized types (e.g., {@code List<Foo>}) are
 * not handled.
 */
public final class FlatteningXmlConverterFactory extends Converter.Factory {
    /** Create an instance using a default {@link Persister} instance for conversion. */
    public static FlatteningXmlConverterFactory create() {
        return create(new Persister());
    }

    /** Create an instance using {@code serializer} for conversion. */
    public static FlatteningXmlConverterFactory create(Serializer serializer) {
        return new FlatteningXmlConverterFactory(serializer, true);
    }

    /** Create an instance using a default {@link Persister} instance for non-strict conversion. */
    public static FlatteningXmlConverterFactory createNonStrict() {
        return createNonStrict(new Persister());
    }

    /** Create an instance using {@code serializer} for non-strict conversion. */
    public static FlatteningXmlConverterFactory createNonStrict(Serializer serializer) {
        return new FlatteningXmlConverterFactory(serializer, false);
    }

    private final Serializer serializer;
    private final boolean strict;

    private FlatteningXmlConverterFactory(Serializer serializer, boolean strict) {
        if (serializer == null) throw new NullPointerException("serializer == null");
        this.serializer = serializer;
        this.strict = strict;
    }

    public boolean isStrict() {
        return strict;
    }

    @Override
    public Converter<ResponseBody, TreeNode> responseBodyConverter(Type type, Annotation[] annotations,
                                                                  Retrofit retrofit) {
        if (!(type instanceof Class)) {
            return null;
        }
        Class<?> cls = (Class<?>) type;
        return new FlatteningXmlResponseBodyConverter(strict);
    }

    @Override
    public Converter<?, RequestBody> requestBodyConverter(Type type,
                                                          Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
        if (!(type instanceof Class)) {
            return null;
        }
        return new FlatteningXmlRequestBodyConverter<>(serializer);
    }
}