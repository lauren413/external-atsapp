package com.accutime.app1.retrofit;

import com.accutime.app1.util.StringUtilities;
import com.accutime.app1.util.Util;

import java.io.IOException;
import okhttp3.ResponseBody;
import org.simpleframework.xml.Serializer;
import retrofit2.Converter;

final class SimpleXmlResponseBodyConverter<T> implements Converter<ResponseBody, T> {
    private final String TAG=SimpleXmlResponseBodyConverter.class.getSimpleName();
    private final Class<T> cls;
    private final Serializer serializer;
    private final boolean strict;

    SimpleXmlResponseBodyConverter(Class<T> cls, Serializer serializer, boolean strict) {
        this.cls = cls;
        this.serializer = serializer;
        this.strict = strict;
    }

    @Override public T convert(ResponseBody value) throws IOException {
        try {
            String respStr = StringUtilities.convertStreamToString(value.byteStream());
            Util.Log.d(TAG, "respStr="+respStr);
            T read = serializer.read(cls, respStr, strict);
            //T read = serializer.read(cls, value.byteStream(), strict);
            if (read == null) {
                throw new IllegalStateException("Could not deserialize body as " + cls);
            }
            return read;
        } catch (RuntimeException | IOException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            value.close();
        }
    }
}