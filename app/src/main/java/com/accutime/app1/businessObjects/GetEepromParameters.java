package com.accutime.app1.businessObjects;

import android.content.Context;
import android.content.Intent;

public class GetEepromParameters  {
    private static final String TAG = GetEepromParameters.class.getSimpleName();

    public static final String RESPONSE_ACTION = "response_from_setup";
    public static final String RESPONSE_PARAM_KEY = "response_key";
    public static final String REQUEST_PARAM_KEY = "request_key";
    public static final String REQUEST_PARAM_ACTION = "request_a_param";
    public static final String REQUEST_KEY = "request_a_key";

    // Use these strings to make requests for specific settings.
    private static final String GET_ORDER_NUMBER = "com.accutime.clocksetup.action.get_order_number";
    public static final String GET_SERIAL_NUMBER = "com.accutime.clocksetup.action.get_serial_number";
    private static final String GET_MAC_NUMBER = "com.accutime.clocksetup.action.get_mac_number";

    private static final String GET_TIME_REGION = "com.accutime.clocksetup.action.get_time_region";
    private static final String GET_TIME_LOCATION = "com.accutime.clocksetup.action.get_time_location";
    private static final String GET_NTP_STATUS = "com.accutime.clocksetup.action.get_enable_ntp";

    private static final String GET_ETHERNET_STATUS = "com.accutime.clocksetup.action.get_enable_ethernet";
    private static final String GET_DHCP_STATUS = "com.accutime.clocksetup.action.get_enable_dhcp";
    private static final String GET_FIXED_IP = "com.accutime.clocksetup.action.get_fixed_ip";
    private static final String GET_SUBNET_MASK = "com.accutime.clocksetup.action.get_subnet_mask";
    private static final String GET_DNS_SERVER = "com.accutime.clocksetup.action.get_dns_server";
    private static final String GET_GATEWAY = "com.accutime.clocksetup.action.get_gateway";

    private static final String GET_WIFI_STATUS = "com.accutime.clocksetup.action.get_enable_wifi";
    private static final String GET_SSID = "com.accutime.clocksetup.action.get_ssid";
    private static final String GET_ENCRYPTION_KEY = "com.accutime.clocksetup.action.get_encryption_key";

    private static final String GET_CELLULAR_STATUS = "com.accutime.clocksetup.action.get_cellular_enable";
    private static final String GET_CELLULAR_SETTINGS = "com.accutime.clocksetup.action.get_cellular_settings";

    private static final String GET_SSH_DAEMON = "com.accutime.clocksetup.action.get_ssh_daemon";
    private static final String GET_ADB_DAEMON = "com.accutime.clocksetup.action.get_adb_daemon";

    private static final String GET_TERMINAL_NAME = "com.accutime.clocksetup.action.get_terminal_name";
    private static final String GET_TERMINAL_TYPE = "com.accutime.clocksetup.action.get_terminal_type";

    private static final String GET_LOG_ENABLE = "com.accutime.clocksetup.action.get_log_enable";
    private static final String GET_LOG_FILESIZE = "com.accutime.clocksetup.action.get_log_filesize";
    private static final String GET_LOG_TIMEINTERVAL = "com.accutime.clocksetup.action.get_log_timeinterval";

    private static final String GET_HOST_USERNAME = "com.accutime.clocksetup.action.get_host_username";
    private static final String GET_HOST_PASSWD = "com.accutime.clocksetup.action.get_host_passwd";
    private static final String GET_HOST_URL = "com.accutime.clocksetup.action.get_host_url";

    private static final String GET_FINGERPRINT_READER = "com.accutime.clocksetup.action.get_fingerprint_reader";
    private static final String GET_FPR_COMPATIBILITY = "com.accutime.clocksetup.action.get_fpr_compatibility";
    private static final String GET_INTERNAL_PROX = "com.accutime.clocksetup.action.get_internal_prox";
    private static final String GET_EXTERNAL_PROX = "com.accutime.clocksetup.action.get_external_prox";
    private static final String GET_OPTICAL_READER = "com.accutime.clocksetup.action.get_optical_reader";
    private static final String GET_MAGNETIC_READER = "com.accutime.clocksetup.action.get_magnetic_reader";

    private static final String GET_KEYPAD_ENABLED = "com.accutime.clocksetup.action.get_keypad_enabled";
    private static final String GET_DIDO_ENABLED = "com.accutime.clocksetup.action.get_dido_Enabled";

    private static final String GET_SYSTEM_LANGUAGE = "com.accutime.clocksetup.action.get_system_language";

    public static void getSerialNumber(Context context) {
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(REQUEST_PARAM_ACTION);
        broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT);
        broadcastIntent.putExtra(REQUEST_KEY, GET_SERIAL_NUMBER);
        context.sendBroadcast(broadcastIntent);
    }
}