package com.accutime.app1.businessObjects;

import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.R;
import com.accutime.app1.activities.BaseActivity;
import com.accutime.app1.activities.MainActivity;
import com.accutime.app1.retrofit.DeviceInterface;
import com.accutime.app1.retrofit.RestAPIs;
import com.accutime.app1.util.MyDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by mkemp on 2/19/18.
 */

public class Biometric {

    private final String TAG = this.getClass().toString();
    private WeakReference<BaseActivity> context;

//    private DeviceInterface deviceInterface;

    public interface BiometricCallback {
        void onFinished();
    }

    // Encode/Decode b64encoded strings
    private String CODES = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";

//    public Biometric(BaseActivity context, DeviceInterface deviceInterface) {
    public Biometric(BaseActivity context) {
        this.context = new WeakReference<>(context);
//        this.deviceInterface = deviceInterface;
    }

    /**
     * Use REST API VerifyAgainstTemplateList to verify the given fingerprint.
     */
    public void verifyAgainstTemplateList(final Employee employee, final String nextState, final String message) {
        Log.i(TAG, "verifyAgainstTemplateList");

        String fingerprintCount = getFingerprintCount(employee);
        String b64Packet = buildBase64Packet(employee, fingerprintCount);

        showFingerprintDialog();

        DeviceInterface deviceInterface = RestAPIs.deviceInterface;
        deviceInterface.VerifyAgainstTemplateList(fingerprintCount, b64Packet).
                enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "Response received");

                String result = null;
                try {

                    if ( response.code() == 200 ) {
                        result = processResponse(response.body().string());
                    }
                    else {
                        String error = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(error);
                        String errorCode = jsonObject.getString("errorCode");
                        result = "Failure [" + errorCode + "]";
                    }

                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                handleResult(result);
            }

            private String processResponse(String response) {
                Log.i(TAG, "processResponse");

                try {

                    JSONObject jsonObject = new JSONObject(response);

                    String theBadge = jsonObject.getString("badge");
                    String theScore = jsonObject.getString("score");

                    String result = "Verify Found Badge [" + theBadge + "], Score [" + theScore + "]";
                    Log.i(TAG, result);
                    return result;

                } catch (JSONException e) {
                    Log.e(TAG, "Error processing the response \n" + e);
                }

                return "Failure";
            }

            private void handleResult(String result) {
                Log.i(TAG, "handleResult");

                if (result != null && !result.equals("Failure")) {

                    // Response was successful. Go ahead and process the badge.
                    Log.i(TAG, "Verification : Success");
//                    context.get().processBadgeEnter(action, employee.badge, employee.name);
//                    ((MainActivity) context.get()).loadMenuActivity(employee);
                    ((MainActivity) context.get()).loadNewState(nextState, message, employee);

                } else {
                    // Don't do anything else.
                    Log.i(TAG, "Verification : Failure");
                    displayToast("Fingerprint does not match.");
                }

                MyDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "onFailure\n" + t);
                displayToast("Scan Failed");
                MyDialog.dismiss();
            }
        });
    }

    private String getFingerprintCount(Employee employee) {
        String fingerprintCount = String.valueOf(employee.fingerPrints.length);
        Log.i(TAG, employee.name + " has " + fingerprintCount + " fingerprints.");
        return fingerprintCount;
    }

    private String buildBase64Packet(Employee employee, String fingerprintCount) {
        byte[] templateArray = buildTemplateArray(employee);
        int length = Integer.valueOf(fingerprintCount) * 2352;
        String b64packet = base64Encode(templateArray, length);
        logBigBuff(b64packet);
        return b64packet;
    }

    private byte[] buildTemplateArray(Employee emp) {
        Log.e(TAG, "buildTemplateArray");

        // Set up internal data
        int numCapturedTemplates = 0;
        byte[] templateArray  = new byte[ 10 * 2352 ];

        // An employee has a bunch of fingerprints associated with them. Get them.
        Employee.FingerPrint[] fingerPrints = emp.fingerPrints;
        //TODO : How many fps are stored?

        // Go through each fingerprint and build a template array.
        for (Employee.FingerPrint fingerPrint : fingerPrints) {

            // Get template
            String templateString = fingerPrint.template;

            // Decode to bytes
            byte[] templateBytes = base64Decode(templateString);

            // Where in memory to start writing
            int s = ( numCapturedTemplates * 2352 );

            try {
                for (int i = 0; i < 2352; i++) {
                    templateArray[s + i] = templateBytes[i];
                }
            } catch (ArrayIndexOutOfBoundsException ex) {
                Log.e(TAG, ex.toString());
            }

            numCapturedTemplates++;
        }

        return templateArray;
    }

    private void showFingerprintDialog() {
        MyDialog.show(
                context.get(),
                context.get().getString(R.string.scanning_fingerprint),
                context.get().getString(R.string.place_finger_on_reader)
        );
    }

    /**
     * Use the Rest API CaptureTemplate() to
     * Scan the user's fingerprint using the fingerprint sensor.
     */
    public void startCaptureScan(final String badgeNumber, final AtsApplication atsApplication,
                                 @Nullable final BiometricCallback biometricCallback) {
        Log.i(TAG, "Starting capture scan.");

        showFingerprintDialog();

        // Pass the badge number into the Rest Api Capture Template
        DeviceInterface deviceInterface = RestAPIs.deviceInterface;
        deviceInterface.CaptureTemplate(badgeNumber).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                Log.i(TAG, "Response received");

                String result = null;
                try {
                    if ( response.code() == 200 ) {
                        result = processResponse(response.body().string());
                    }
                    else {
                        String error = response.errorBody().string();
                        JSONObject jsonObject = new JSONObject(error);
                        String errorCode = jsonObject.getString("errorCode");
                        result = "Error [" + errorCode + "]";
                    }
                } catch (IOException | JSONException e) {
                    e.printStackTrace();
                }

                handleResult(result);
            }

            /**
             * Process the response body string.
             * @return the fingerprint packet as a string.
             */
            private String processResponse(String response) {

                Log.i(TAG, "Processing response");

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray packets = jsonObject.getJSONArray("packets");

                    String b64packet = (String) packets.get(0);
                    Log.i(TAG, "Resulting b64encoded packet: \n" + b64packet);

                    String theQuality = jsonObject.getString("Quality");
                    displayToast("New Fingerprint Template Stored : Quality [" + theQuality + "]");

                    return b64packet;

                } catch (JSONException e) {
                    Log.e(TAG, "Error processing the response");
                    Toast.makeText(context.get(), "Error processing the response", Toast.LENGTH_SHORT).show();
                }

                return "Failure";
            }

            private void handleResult(String result) {
                Log.i(TAG, "handleResult");

                if ((result != null && !result.equals("Failure"))) {
                    Log.d(TAG, "Adding badge " + badgeNumber);
                    atsApplication.addTemplateToUser(badgeNumber, result);
                    if (biometricCallback != null) {
                        biometricCallback.onFinished();
                    }
                }

                else {
                    Log.i(TAG, "Fingerprint scan failed");
                    displayToast("Scan failed");
                    if (biometricCallback != null) {
                        biometricCallback.onFinished();
                    }
                }

                MyDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e(TAG, "Response failed");
                displayToast("Response failed");
                MyDialog.dismiss();
            }
        });
    }

    private void logBigBuff (String buf) {
        int blen = 128;
        if (buf.length() > blen) {
            Log.i(TAG, buf.substring(0, blen));
            logBigBuff(buf.substring(blen));
        } else {
            Log.i(TAG, buf);
        }
    }

    private byte[] base64Decode(String input) {
        Log.i(TAG, "base64Decode gets input string of " + input.length() + " chars");
        int len = input.length();
        int last = (int) input.charAt(len-1);
        if ( last < 33 ) {
            len--;
            Log.i(TAG, "Found a trailing non-printable char, only processing " + len + " chars");
        }
        if (len % 4 == 0)    {
            byte decoded[] = new byte[((len * 3) / 4) - (input.indexOf('=') > 0 ? (len - input.indexOf('=')) : 0)];
            char[] inChars = input.toCharArray();
            Log.i(TAG, "B64 Decode from " + len + " to " + decoded.length + " bytes");
            int j = 0;
            int b[] = new int[4];
            for (int i = 0; i < len; i += 4)     {
                b[0] = CODES.indexOf(inChars[i]);
                b[1] = CODES.indexOf(inChars[i + 1]);
                b[2] = CODES.indexOf(inChars[i + 2]);
                b[3] = CODES.indexOf(inChars[i + 3]);
                decoded[j++] = (byte) ((b[0] << 2) | (b[1] >> 4));
                if (b[2] < 64)      {
                    decoded[j++] = (byte) ((b[1] << 4) | (b[2] >> 2));
                    if (b[3] < 64)  {
                        decoded[j++] = (byte) ((b[2] << 6) | b[3]);
                    }
                }
            }
            return decoded;
        } else {
            throw new IllegalArgumentException("Invalid base64 input");
        }
    }

    private String base64Encode(byte[] in, int len) {
        Log.i(TAG, "base64Encode gets to use " + len + " from an input array of " + in.length + " bytes");
        StringBuilder out = new StringBuilder((len * 4) / 3);
        int b;
        for (int i = 0; i < len; i += 3)  {
            b = (in[i] & 0xFC) >> 2;
            out.append(CODES.charAt(b));
            b = (in[i] & 0x03) << 4;
            if (i + 1 < in.length)      {
                b |= (in[i + 1] & 0xF0) >> 4;
                out.append(CODES.charAt(b));
                b = (in[i + 1] & 0x0F) << 2;
                if (i + 2 < len)  {
                    b |= (in[i + 2] & 0xC0) >> 6;
                    out.append(CODES.charAt(b));
                    b = in[i + 2] & 0x3F;
                    out.append(CODES.charAt(b));
                } else  {
                    out.append(CODES.charAt(b));
                    out.append('=');
                }
            } else      {
                out.append(CODES.charAt(b));
                out.append("==");
            }
        }

        Log.i(TAG, "B64 Encode from " + len + " bytes to " + out.length() + " chars" );
        return out.toString();
    }

    /**
     * Display message to the user.
     * @param text : the message to show
     */
    private void displayToast(String text) {
        Log.d(TAG, "displayToast");

        Toast toast = Toast.makeText(context.get(), text, Toast.LENGTH_LONG);
        LinearLayout toastLayout = (LinearLayout) toast.getView();
        TextView toastTV = (TextView) toastLayout.getChildAt(0);
        toastTV.setTextSize(28);
        toast.show();
    }
}
