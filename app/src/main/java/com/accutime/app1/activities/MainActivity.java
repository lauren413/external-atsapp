package com.accutime.app1.activities;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.R;
import com.accutime.app1.businessObjects.Biometric;
import com.accutime.app1.businessObjects.Employee;
import com.accutime.app1.businessObjects.GetEepromParameters;
import com.accutime.app1.businessObjects.State;
import com.accutime.app1.common.Constants;
import com.accutime.app1.loadables.DeviceDiscoverLoadable;
import com.accutime.app1.loader.Loader;
import com.accutime.app1.receivers.EepromParamReceiver;
import com.accutime.app1.receivers.EmployeeDataReceiver;
import com.accutime.app1.services.DatabaseService;
import com.accutime.app1.services.DeviceService;
import com.accutime.app1.util.MyDialog;
import com.accutime.app1.util.MyJobScheduler;
import com.accutime.app1.util.Util;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import static com.accutime.app1.businessObjects.GetEepromParameters.GET_SERIAL_NUMBER;
import static com.accutime.app1.businessObjects.GetEepromParameters.RESPONSE_ACTION;
import static com.accutime.app1.common.Constants.EMPLOYEE_EXTRA;
import static com.accutime.app1.common.Constants.KEY_ACTION;
import static com.accutime.app1.common.Constants.KEY_BADGE;
import static com.accutime.app1.common.Constants.KEY_HINT;
import static com.accutime.app1.common.Constants.KEY_LOGIN;
import static com.accutime.app1.common.Constants.KEY_MAIN_STATE;
import static com.accutime.app1.common.Constants.KEY_NEXTSTATE;
import static com.accutime.app1.common.Constants.KEY_POST_ACTION_MESSAGE;
import static com.accutime.app1.common.Constants.STATE_EXTRA;
import static com.accutime.app1.common.Constants.TEXT_FIELD_KEY;
import static com.accutime.app1.services.DatabaseService.READ_EMPLOYEE_FROM_DATABASE_ACTION;

/**
 * This is the home activity.
 * It is what the customer will see when they approach the terminal.
 * They can punch in from here and access the rest of the app.
 */
public class MainActivity extends BaseActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    private Context context = MainActivity.this;

    private EditText mViewEditText;

    public static Loader loader = new Loader("LoaderThread");

    private Biometric biometricObject;
    private State mState;

    /* Get employee from DatabaseService when someone tries to punch in. */
    private EmployeeDataReceiver employeeDataReceiver = new EmployeeDataReceiver() {
        @Override
        public void performEmployeeProcessing(Bundle extra) {
            MainActivity.this.performEmployeeProcessing(extra);
        }
    };

    /* Get eeprom parameter when requested. */
    private EepromParamReceiver eepromParamReceiver = new EepromParamReceiver() {
        @Override
        public void handleEepromParam(String request, String result) {
            Log.d(TAG, request + " : " + result);
            if (request.equals(GET_SERIAL_NUMBER)) {
                Constants.SERIAL_NUMBER = result;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initializeConstants();
        initializeState();

        Log.d(TAG, "MainActivity onCreate() started.");

        // Start database service which will later read employees.
        startService(new Intent(this, DatabaseService.class));

        // TODO : Disabled for now
        //startService(new Intent(this, DeviceService.class));

        // Start calling out to EXML on a schedule.
        startLoadables();
        setupTextViews();
        getEepromParams();

        biometricObject = new Biometric(this);

        Button debugPunchIn = (Button) findViewById(R.id.btn_punch_in);
        if (debugPunchIn != null) {
            debugPunchIn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    setBadgeNumber("021111");
                    String nextState = mState.getAttribute(TEXT_FIELD_KEY, KEY_NEXTSTATE);
                    String message = mState.getAttribute(TEXT_FIELD_KEY, KEY_POST_ACTION_MESSAGE);
                    readBadgeField(nextState, message);
                }
            });
        }
    }

    private String getBadgeNumber() {
        return mViewEditText.getText().toString();
    }
    private void setBadgeNumber(String value) {
        mViewEditText.setText(value);
    }

    /**
     * Set constants in constants file to value from internal storage.
     */
    private void initializeConstants() {

        // Fetch file and convert to json
        String configFile = Util.getFileFromInternalStorage(Constants.CONFIG_FILENAME);
        JSONObject configJson = Util.convertToJson(configFile);
        if (Util.objectExists(configJson))
        {
            // Set constant values for app
            Constants.setConstants(configJson);
        }
        else {
            Log.e(TAG, getString(R.string.config_file_not_valid_json));
            MyDialog.show(context, getString(R.string.invalid_config_file_loading_defaults));
        }
    }

    /**
     * Set up activity based on profile found in internal storage.
     */
    private void initializeState() {

        // Look at file
        String profile = Util.getFileFromInternalStorage(Constants.PROFILE_FILENAME);

        // Find state. If it doesn't exist, load default screen.
        if (profile.isEmpty()) {
            setContentView(R.layout.no_profile);
        }

        // If state exists, set up application layout and workflow accordingly.
        else
        {
            // Create state
            String desiredState = KEY_MAIN_STATE;
            mState = new State(profile, desiredState);

            // If this state does not exist in the profile, show dialog and go back.
            if (!mState.thisStateExists()) {
                Util.showStateDoesNotExistDialog(context, desiredState);
                return;
            }

            // Set layout
            setContentView(getLayoutID(mState.getLayout()));

            // Set label
            String label = mState.getLabel();
            if (! Util.objectExists(label))
            {
                // Default label
                label = getString(R.string.welcome);
            }

            TextView tvLabel = (TextView) findViewById(R.id.tv_label);
            tvLabel.setText(label);
        }

    }

    private void startLoadables() {
        Log.v(TAG, "startLoadables");

        // Schedule for heartbeat and employees
        MyJobScheduler.scheduleHeartbeatJob(context);
        MyJobScheduler.schedulePunchJob(context);

        // Start loading
        loader.startLoading(new DeviceDiscoverLoadable(this), new Loader.Callback() {
            @Override
            public void onLoadError(Loader.Loadable loadable, IOException exception) {
                Log.e(TAG, "Discover devices got error : ", exception);
            }
            @Override public void onLoadCanceled(Loader.Loadable loadable) {}
            @Override public void onLoadCompleted(Loader.Loadable loadable) {}
        });
    }

    private void setupTextViews() {
        if (mState == null || !mState.containsTextField()) {
            return;
        }

        mViewEditText = (EditText) findViewById(R.id.et_badge_number);
        mViewEditText.setHint(mState.getAttribute(TEXT_FIELD_KEY, KEY_HINT));
        mViewEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            public boolean onEditorAction(TextView exampleView, int actionId, KeyEvent event) {
                switch (actionId) {
                    // On enter click
                    case EditorInfo.IME_ACTION_DONE:
                        switch (mState.getAttribute(TEXT_FIELD_KEY, KEY_ACTION)) {
                            case KEY_LOGIN:
                                // Login
                                String nextState = mState.getAttribute(TEXT_FIELD_KEY, KEY_NEXTSTATE);
                                String message = mState.getAttribute(TEXT_FIELD_KEY, KEY_POST_ACTION_MESSAGE);
                                readBadgeField(nextState, message);
                                break;
                            default:
                                // Hide keyboard
                                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                                if (imm != null) {imm.hideSoftInputFromWindow(mViewEditText.getWindowToken(), 0);}
                                break;
                        }
                        return true;
                }
                return false;
            }
        });
    }

    /**
     * Send a broadcast to clock setup to get eeprom params.
     * They will be saved to constants.
     */
    public void getEepromParams() {
        // External broadcast
        IntentFilter filter = new IntentFilter(RESPONSE_ACTION);
        filter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(eepromParamReceiver, filter);

        // Populate constant serial number
        GetEepromParameters.getSerialNumber(context);
    }

    /**
     * Calls performBadgeProcessing
     */
    private void readBadgeField(String nextState, String postActionMessage) {
        Log.v(TAG, "readBadgeField");
        String badgeNumber = getBadgeNumber();

        Bundle bundle = new Bundle();
        bundle.putString(KEY_NEXTSTATE, nextState);
        bundle.putString(KEY_POST_ACTION_MESSAGE, postActionMessage);
        processBadgeNumber(badgeNumber, bundle);

        setBadgeNumber("");
    }

    /**
     * This is called when a badge number is entered.
     */
    public void processBadgeNumber(String badgeNumber, Bundle extra) {
        Log.v(TAG, "performBadgeProcessing");

        // Check for invalid entry
        if (TextUtils.isEmpty(badgeNumber)) {
            Util.displayToast(context, "Badge number is empty.");
            return;
        }

        // Put badge number in data bundle
        if (extra == null) { extra = new Bundle(); }
        extra.putString(KEY_BADGE, badgeNumber);

        // Broadcast to Database Service to process
        Intent intent = new Intent(READ_EMPLOYEE_FROM_DATABASE_ACTION);
        intent.putExtra(DatabaseService.EMPLOYEE_BUNDLE_KEY, extra);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }

    /**
     * When DatabaseService broadcasts an employee to MainActivity, this is then invoked.
     * Process that employee.
     */
    public void performEmployeeProcessing(Bundle extra) {
        Log.v(TAG, "performEmployeeProcessing: " + extra.toString());

        Employee employee = extra.getParcelable(EMPLOYEE_EXTRA);
        if (employee == null || TextUtils.isEmpty(employee.name)) {
            Util.displayToast(context, getString(R.string.employee_not_found));
            return;
        }

        String nextState = extra.getString(KEY_NEXTSTATE);
        if (nextState == null || TextUtils.isEmpty(nextState)) {
            Util.displayToast(context, "Next State not found");
            return;
        }

        String message = extra.getString(KEY_POST_ACTION_MESSAGE);

        Log.v(TAG, String.format("Got badge # %s for employee %s", extra.getString(KEY_BADGE), employee.name));

        if (!checkIfFingerprintExists(employee))
        {
            // No fingerprint to scan? Load new state right away.
            loadNewState(nextState, message, employee);
        }
        else
        {
            // If employee has a fingerprint, first verify and the load new state.
            biometricObject.verifyAgainstTemplateList(employee, nextState, message);
        }
    }

    /**
     * See if the given employee has a fingerprint associated with them.
     */
    private boolean checkIfFingerprintExists(Employee employee) {
        if (employee != null && employee.fingerPrints != null && employee.fingerPrints.length > 0)
        {
            Log.v(TAG, "Employee " + employee.name + " has " + employee.fingerPrints.length + " fingerprint(s).");

            for (Employee.FingerPrint fingerPrint : employee.fingerPrints)
            {
                if (fingerPrint.userId != -1 || !TextUtils.isEmpty(fingerPrint.template)) {
                    // Found at least one fingerprint.
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Load next state from JSON.
     */
    public void loadNewState(String nextState, String message, Employee employee) {
        Intent intent = new Intent(context, StateActivity.class);
        intent.putExtra(STATE_EXTRA, nextState);
        intent.putExtra(EMPLOYEE_EXTRA, employee);
        startActivity(intent);

        if (message != null) {
            Util.displayToast(context, message);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // Internal broadcast
        LocalBroadcastManager.getInstance(this).registerReceiver(employeeDataReceiver,
                new IntentFilter(DatabaseService.EMPLOYEE_DATA_RESPONSE));
    }

    @Override
    protected void onStop() {
        super.onStop();

        LocalBroadcastManager.getInstance(this).unregisterReceiver(employeeDataReceiver);
        unregisterReceiver(eepromParamReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        stopService(new Intent(this, DatabaseService.class));
        stopService(new Intent(this, DeviceService.class));
    }

    // For debugging
    public void showAllEmployees(View view) {
        Log.v(TAG, "Show all employees");

        Map<String, byte[]> pairs = ((AtsApplication) getApplication())
                .getKeyValuePairsForNode("/Employees/Employee/badge");

        if (pairs == null) { Log.e(TAG, "pairs is null"); return; }

        Set keyset = pairs.keySet();
        if (keyset.isEmpty()) { Log.e(TAG, "keyset is empty"); return; }


        for (Object key : keyset) {
            Log.v(TAG, "Badge: " + key);
            processBadgeNumber((String) key, null);
        }
    }

    // These aren't used here but need to be initialized.
    @Override public void updateTimerView(String time) {}
    @Override public void showTimer() {}
    @Override public void hideTimer() {}
    @Override public void loadNewState(String timeoutState) {}
}
