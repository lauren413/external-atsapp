package com.accutime.app1.loadables;

/**
 * Created by mkemp on 2/19/18.
 */

import android.text.TextUtils;
import android.util.Log;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.businessObjects.Employee;
import com.accutime.app1.loader.Loader;
import com.accutime.app1.xmlparser.Attribute;
import com.accutime.app1.xmlparser.NamedEntity;
import com.accutime.app1.xmlparser.TagNode;
import com.accutime.app1.xmlparser.XmlUtils;
import com.snappydb.SnappydbException;

import java.io.UnsupportedEncodingException;
import java.util.Collection;

/**
 * Given a badge number, search the database and construct an employee.
 */
public class ReadEmployeeFromDatabaseLoadable implements Loader.Loadable{

    public static final String TAG = ReadEmployeeFromDatabaseLoadable.class.getSimpleName();

    public final Employee employee = new Employee();

    private String badge;
    private AtsApplication atsApplication;

    public ReadEmployeeFromDatabaseLoadable(String badge, AtsApplication atsApplication) {
        this.badge = badge;
        this.atsApplication = atsApplication;
    }

    public void load() {
        Log.v(TAG, "load");

        employee.name = null;
        employee.badge = badge;
        if (!TextUtils.isEmpty(badge)) {
            Log.d(TAG, "Badge is " + badge);
            if (atsApplication.badgeNodes == null || atsApplication.badgeNodes.keySet().size() == 0) {
                Log.e(TAG, "No badge nodes");
                return;
            }
            byte[] nodeId = atsApplication.badgeNodes.get(badge);
            XmlUtils.ValueTuple employeeVt = null;
            TagNode employeeNode = null;

            if (nodeId != null) {
                Log.v(TAG, "Node ID not null");
                try {
                    employeeVt = XmlUtils.getParent(nodeId, atsApplication.getDbRelations());
                    employeeNode = (TagNode) XmlUtils.retrieveObject(employeeVt.nodeId, atsApplication.getDbIdToPath(),
                            atsApplication.getDbPathToId(), atsApplication.getDbRelations());
                    if (employeeNode != null) {
                        employee.badge = badge;
                        employee.name = XmlUtils.getNodeAttribute(employeeNode, "name");
                        employee.supervisor_level = XmlUtils.getNodeAttribute(employeeNode, "supervisor_level");
                        Collection<NamedEntity> namedEntities = employeeNode.findChildren("FingerPrint");
                        employee.fingerPrints = new Employee.FingerPrint[namedEntities.size()];
                        int index = 0;
                        for(NamedEntity n: namedEntities) {
                            if (n instanceof TagNode) {
                                int userId = -1;
                                int scanQuality = -1;
                                String device = null;
                                String template = null;
                                Attribute a = ((TagNode) n).findAttribute("userId");
                                if (a != null && !TextUtils.isEmpty(a.getValue())){
                                    userId = Integer.parseInt(a.getValue());
                                }
                                a = ((TagNode) n).findAttribute("scanQuality");
                                if (a != null && !TextUtils.isEmpty(a.getValue())){
                                    scanQuality = Integer.parseInt(a.getValue());
                                }
                                a = ((TagNode) n).findAttribute("device");
                                if (a != null && !TextUtils.isEmpty(a.getValue())){
                                    device=a.getValue();
                                }
                                a = ((TagNode) n).findAttribute("template");
                                if (a != null && !TextUtils.isEmpty(a.getValue())){
                                    template=a.getValue();
                                }
                                Employee.FingerPrint fingerPrint = new Employee.FingerPrint(scanQuality, userId, device, template);
                                employee.fingerPrints[index] = (fingerPrint);
                                index++;
                            }
                        }
                    }
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                } catch (SnappydbException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e(TAG, "Node ID is null");
            }
        }
    }

    @Override public void cancelLoad() { }
    @Override public boolean isLoadCanceled() { return false; }
}