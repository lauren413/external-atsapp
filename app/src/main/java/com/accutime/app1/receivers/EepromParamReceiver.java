package com.accutime.app1.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.accutime.app1.businessObjects.GetEepromParameters;

/**
 * Created by mkemp on 6/19/18.
 */

public abstract class EepromParamReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Got string containing eeprom param.
        String request = intent.getStringExtra(GetEepromParameters.REQUEST_PARAM_KEY);
        String result = intent.getStringExtra(GetEepromParameters.RESPONSE_PARAM_KEY);
        handleEepromParam(request, result);
    }

    /**
     * Classes can implement this method to perform employee processing however they need.
     */
    public abstract void handleEepromParam(String request, String paramValue);
}
