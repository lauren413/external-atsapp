package com.accutime.app1.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.accutime.app1.services.DatabaseService;

/**
 * Created by mkemp on 6/19/18.
 */

public abstract class EmployeeDataReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Got a bundle containing employee data.
        Bundle bundle = intent.getBundleExtra(DatabaseService.EMPLOYEE_BUNDLE_KEY);
        performEmployeeProcessing(bundle);
    }

    /**
     * Classes can implement this method to perform employee processing however they need.
     */
    public abstract void performEmployeeProcessing(Bundle bundle);
}
