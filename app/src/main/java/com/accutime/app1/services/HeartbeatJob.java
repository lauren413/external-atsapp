package com.accutime.app1.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.common.Constants;
import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.retrofit.RestAPIs;
import com.accutime.app1.util.Measurement;
import com.accutime.app1.util.MyDialog;
import com.accutime.app1.util.MyJobScheduler;
import com.accutime.app1.util.PreferenceUtilities;
import com.accutime.app1.util.Util;
import com.accutime.app1.util.XMLParser;
import com.accutime.app1.xmlparser.TagNode;
import com.accutime.app1.xmlparser.TreeNode;
import com.accutime.app1.xmlparser.TreeToXML;
import com.accutime.app1.xmlparser.XmlUtils;
import com.snappydb.SnappydbException;

import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.gzip.GzipCompressorInputStream;
import org.w3c.dom.Document;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * JobService to be scheduled by the MyJobScheduler.
 * start another service
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class HeartbeatJob extends JobService {

    private static final String TAG = HeartbeatJob.class.getSimpleName();
    private Context context = HeartbeatJob.this;

    private static final String VERSION_TAG = "Version";

    static final String CONFIG_KEY = "Config";
    private static final String DLD_KEY = "Dld";
    private static final String FLASH_KEY = "Flash";
    private static final String EMPLOYEE_KEY = "Employee";
    private static final String BIOMETRICS_KEY = "Biometrics";

    private static final String EMPLOYEE_FILENAME = "EmployeeFile";
    private static final String BIOMETRICS_FILENAME = "BiometricsFile";

    private AtsService atsService;

    @Override
    public boolean onStartJob(final JobParameters params) {

        Log.i(TAG, "onStartJob");
        atsService = RestAPIs.atsService;
        if (atsService == null) return false;

        final Measurement heartbeatMeasurement = new Measurement();
        heartbeatMeasurement.start();

        Call<TagNode> heartbeatCall = atsService.heartbeatRawCallback(
                PreferenceUtilities.Preferences.getConfigVersion(context),
                PreferenceUtilities.Preferences.getDldVersion(context),
                PreferenceUtilities.Preferences.getFlashVersion(context),
                PreferenceUtilities.Preferences.getEmployeeVersion(context),
                PreferenceUtilities.Preferences.getBiometricsVersion(context)
        );
        Log.i(TAG, String.valueOf(heartbeatCall.request().url()));
        heartbeatCall.enqueue(new Callback<TagNode>() {
            @Override
            public void onResponse(Call<TagNode> call, Response<TagNode> response) {
                heartbeatMeasurement.end();
                Log.d(TAG, "Got heartbeat. Time = " + heartbeatMeasurement.end +
                        ", Diff = " + heartbeatMeasurement.diff() +
                        ", Avg = " + heartbeatMeasurement.avg);

                if (response == null) { Log.e(TAG, "Null response."); return; }
                if (response.body() == null) { Log.e(TAG, "Null body"); return; }

                Context context = getApplicationContext();
                AtsApplication atsApplication = (AtsApplication) context;
                XmlUtils.ValueTuple rootNode = null;

                try {

                    rootNode = atsApplication.storeObject(response.body(), "");
                    rootNode.commitRelation(atsApplication.getDbRelations());
                    atsApplication.storeRootNodeName(rootNode);

                    TreeNode tnode = XmlUtils.retrieveObject(rootNode.nodeId,
                            atsApplication.getDbIdToPath(),
                            atsApplication.getDbPathToId(),
                            atsApplication.getDbRelations()
                    );

                    if (tnode != null) {
                        TreeToXML txml = new TreeToXML(tnode);
                        Util.Log.d(TAG, "Back to xml: " + txml);

                        // Check versions and update if necessary
                        checkHeartbeatVersions(txml.toString());
                    }

                    // TODO this should happen even when I do have employees...
                    if (!PreferenceUtilities.Preferences.getHasEmployees(context)) {

                        // Now that heartbeat was successful, get employees.
                        // Do it from here so that it can be rescheduled if necessary.
                        MyJobScheduler.scheduleGetEmployeesJob(context);

                    }

                } catch (SnappydbException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

                jobFinished(params, false);

            }

            @Override
            public void onFailure(Call<TagNode> call, Throwable t) {
                heartbeatMeasurement.end();
                Log.e(TAG, "Failed to get heartbeat. Time = " + heartbeatMeasurement.end +
                        ", Diff = " + heartbeatMeasurement.diff() +
                        ", Avg = " + heartbeatMeasurement.avg);
                Log.e(TAG, "Error = ", t);
                jobFinished(params, false);
            }

        });

        // There's still work to be done
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
//        if (heartbeatCall != null) heartbeatCall.cancel();

        // Please reschedule
        return true;
    }

    /**
     * Check heartbeat versions. If they do not match what we have, upgrade.
     */
    private void checkHeartbeatVersions(String heartbeatXML) {
        Log.d(TAG, "Got heartbeat string " + heartbeatXML);

        // Get full XML document
        Document document = XMLParser.getDomElement(heartbeatXML);
        if (document != null)
        {
            // Parse out versions from xml
            final String HBConfigVersion = XMLParser.findValueInXML(document, VERSION_TAG, CONFIG_KEY);
            final String HBDldVersion = XMLParser.findValueInXML(document, VERSION_TAG, DLD_KEY);
            final String HBFlashVersion = XMLParser.findValueInXML(document, VERSION_TAG, FLASH_KEY);
            final String HBEmployeeVersion = XMLParser.findValueInXML(document, VERSION_TAG, EMPLOYEE_KEY);
            final String HBBiometricsVersion = XMLParser.findValueInXML(document, VERSION_TAG, BIOMETRICS_KEY);

            Log.d(TAG, "Got Config Version " + HBConfigVersion + " from XML");
            Log.d(TAG, "Got Dld Version " + HBDldVersion + " from XML");
            Log.d(TAG, "Got Flash Version " + HBFlashVersion + " from XML");
            Log.d(TAG, "Got Employee Version " + HBEmployeeVersion + " from XML");
            Log.d(TAG, "Got Biometrics Version " + HBBiometricsVersion + " from XML");

            final String currentConfigVersion = getCurrentVersionFromPrefs(context, CONFIG_KEY);
            final String currentDldVersion = getCurrentVersionFromPrefs(context, DLD_KEY);
            final String currentFlashVersion = getCurrentVersionFromPrefs(context, FLASH_KEY);

            Log.d(TAG, "Current Config Version: " + currentConfigVersion + " from XML");
            Log.d(TAG, "Current Dld Version: " + currentDldVersion + " from XML");
            Log.d(TAG, "Current Flash Version: " + currentFlashVersion + " from XML");

            if (needsUpdate(currentConfigVersion, HBConfigVersion)) {
                updateConfigVersion(HBConfigVersion);
            }

            if (needsUpdate(currentDldVersion, HBDldVersion)) {
                updateProfileVersion(HBDldVersion);
            }

            if (needsUpdate(currentFlashVersion, HBFlashVersion)) {
                updateFlashVersion(HBFlashVersion);
            }

            updateEmployeeVersion(HBEmployeeVersion);
            updateBiometricsVersion(HBBiometricsVersion);

//                    postBiometricsTest();
        }
        else {
            Log.e(TAG, "XML from Middleware has an error. Aborting.");
        }
    }

    /**
     * Check current config version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateConfigVersion(final String desiredVersion)
    {
        Log.d(TAG, "Config version NOT up to date. Downloading new version...");
        Call<ResponseBody> call = atsService.configurationRequest(desiredVersion);
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                boolean writeSuccess = writeResponseBodyToDisk(response.body(), Constants.CONFIG_FILENAME);
                if (writeSuccess) saveNewVersionNameToPrefs(context, CONFIG_KEY, desiredVersion);
                else Log.e(TAG, "Error writing file!");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to download config file " + desiredVersion);
                Log.e(TAG, throwable.toString());
            }
        });
    }

    /**
     * Check current download version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateProfileVersion(final String desiredVersion)
    {
        Log.d(TAG, "Dld version NOT up to date. Downloading new version...");
        Call<ResponseBody> call = atsService.downloadRequest(desiredVersion);
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                boolean writeSuccess = writeResponseBodyToDisk(response.body(), Constants.PROFILE_FILENAME);
                if (writeSuccess) saveNewVersionNameToPrefs(context, DLD_KEY, desiredVersion);
                else Log.e(TAG, "Error writing file!");
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to download dld file" + desiredVersion);
                Log.e(TAG, throwable.toString());
            }
        });
    }

    /**
     * TODO Resources tar download based off this
     * Check current flash version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateFlashVersion(final String desiredVersion)
    {
        Log.d(TAG, "Flash version NOT up to date. Downloading new version...");
        Call<ResponseBody> call = atsService.flashRequest(desiredVersion);
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
                // When streaming, the response is a stream of network data... need new thread.
                new WriteFlashResourcesTask(context, desiredVersion).execute(response.body());
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to download flash file" + desiredVersion);
                Log.e(TAG, throwable.toString());
            }
        });
    }

    /**
     * This task is responsible for writing the large tar file off the main thread.
     */
    private static class WriteFlashResourcesTask extends AsyncTask<ResponseBody, Void, Boolean> {

        private WeakReference<Context> contextWeakReference;
        private String desiredVersion;

        WriteFlashResourcesTask(Context context, String desiredVersionName) {
            contextWeakReference = new WeakReference<>(context);
            desiredVersion = desiredVersionName;
        }

        @Override
        protected Boolean doInBackground(ResponseBody... voids) {
            return writeResponseBodyToDisk(voids[0], Constants.FLASH_FILENAME);
        }

        @Override
        protected void onPostExecute(Boolean writeSuccess) {
            if (writeSuccess) {
                saveNewVersionNameToPrefs(contextWeakReference.get(), FLASH_KEY, desiredVersion);

                File flashFile = new File(Environment.getExternalStorageDirectory() + "/FlashFile");
                File unzippedFlashFile = new File(
                        Environment.getExternalStorageDirectory() + "/UnzippedFlashFile");

                // This downloads successfully!
                // TODO : Base resources off what is found in this file.

                try {
                    uncompressTarGZ(flashFile, unzippedFlashFile);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            else Log.e(TAG, "Error writing file!");
        }
    }

    public static void uncompressTarGZ(File tarFile, File dest) throws IOException {
        dest.mkdir();
        TarArchiveInputStream tarIn;

        tarIn = new TarArchiveInputStream(
                new GzipCompressorInputStream(
                        new BufferedInputStream(
                                new FileInputStream(
                                        tarFile
                                )
                        )
                )
        );

        TarArchiveEntry tarEntry = tarIn.getNextTarEntry();

        // tarIn is a TarArchiveInputStream
        while (tarEntry != null) {// create a file with the same name as the tarEntry
            File destPath = new File(dest, tarEntry.getName());
            System.out.println("working: " + destPath.getCanonicalPath());
            if (tarEntry.isDirectory()) {
                destPath.mkdirs();
            } else {
                destPath.createNewFile();
                //byte [] btoRead = new byte[(int)tarEntry.getSize()];
                byte [] btoRead = new byte[1024];
                //FileInputStream fin
                //  = new FileInputStream(destPath.getCanonicalPath());
                BufferedOutputStream bout =
                        new BufferedOutputStream(new FileOutputStream(destPath));
                int len = 0;

                while((len = tarIn.read(btoRead)) != -1)
                {
                    bout.write(btoRead,0,len);
                }

                bout.close();
                btoRead = null;

            }
            tarEntry = tarIn.getNextTarEntry();
        }
        tarIn.close();
    }
    /**
     * If the two given versions do not match, we need to upgrade.
     * @param currentVersion current version from shared prefs
     * @param givenVersion version from EXML
     * @return whether upgrade is required
     */
    boolean needsUpdate(String currentVersion, String givenVersion) {
        return !givenVersion.equals(currentVersion);
    }

    /**
     * TODO App apk download based off this
     * Check current app version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateAppVersion(final String desiredVersion)
    {
//        // Check current version
//        String myFlashVersion = PreferenceUtilities.Preferences.getFlashVersion(context);
//        if (myFlashVersion.equals(desiredVersion)) {
//            Log.d(TAG, "Flash version up to date.");
//            return;
//        } else { Log.i(TAG, "Flash version NOT up to date. Downloading new version..."); }
//
//        // Download update
//        Call<ResponseBody> call = atsService.flashRequest(desiredVersion);
//        call.enqueue (new Callback<ResponseBody>() {
//            @SuppressLint("StaticFieldLeak")
//            @Override
//            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
//                // When streaming, the response is a stream of network data...
//                // Need a new thread
//                new AsyncTask<Void, Void, Void>() {
//                    @Override
//                    protected Void doInBackground(Void... voids)
//                    {
//                        boolean downloadSuccess = writeResponseBodyToDisk(response.body(), FLASH_FILENAME);
//                        if (downloadSuccess) {
//                            PreferenceUtilities.Preferences.setFlashVersion(context, desiredVersion);
//                            Log.i(TAG, "Updated flash version");
//                        }
//                        return null;
//                    }
//                }.execute();
//            }
//            @Override public void onFailure(Call<ResponseBody> call, Throwable throwable) {
//                Log.e(TAG, "Failed to download flash file" + desiredVersion);
//                Log.e(TAG, throwable.toString());
//            }
//        });
    }

    /**
     * TODO OS download based off this
     * / TODO Why bother with this herer??
     * Check current os version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateOSVersion(final String desiredVersion)
    {
//        // Check current version
//        String myFlashVersion = PreferenceUtilities.Preferences.getFlashVersion(context);
//        if (myFlashVersion.equals(desiredVersion)) {
//            Log.d(TAG, "Flash version up to date.");
//            return;
//        } else { Log.i(TAG, "Flash version NOT up to date. Downloading new version..."); }
//
//        // Download update
//        Call<ResponseBody> call = atsService.flashRequest(desiredVersion);
//        call.enqueue (new Callback<ResponseBody>() {
//            @SuppressLint("StaticFieldLeak")
//            @Override
//            public void onResponse(Call<ResponseBody> call, final Response<ResponseBody> response) {
//                // When streaming, the response is a stream of network data...
//                // Need a new thread
//                new AsyncTask<Void, Void, Void>() {
//                    @Override
//                    protected Void doInBackground(Void... voids)
//                    {
//                        boolean downloadSuccess = writeResponseBodyToDisk(response.body(), FLASH_FILENAME);
//                        if (downloadSuccess) {
//                            PreferenceUtilities.Preferences.setFlashVersion(context, desiredVersion);
//                            Log.i(TAG, "Updated flash version");
//                        }
//                        return null;
//                    }
//                }.execute();
//            }
//            @Override public void onFailure(Call<ResponseBody> call, Throwable throwable) {
//                Log.e(TAG, "Failed to download flash file" + desiredVersion);
//                Log.e(TAG, throwable.toString());
//            }
//        });
    }

    /**
     * TODO Update employee list based off this
     * Check current employee version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateEmployeeVersion(final String desiredVersion)
    {
        // Check current version
        String myEmployeeVersion = PreferenceUtilities.Preferences.getEmployeeVersion(context);
        if (myEmployeeVersion.equals(desiredVersion)) {
            Log.d(TAG, "Employee version up to date.");
            return;
        } else { Log.i(TAG, "Employee version NOT up to date. Downloading new version..."); }

        // Download update
        // TODO : Should not be hardcoded
        String terminalId = "123456";
        Call<ResponseBody> call = atsService.employeeRequestComplete(
                terminalId, desiredVersion, myEmployeeVersion
        );
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                boolean downloadSuccess = writeResponseBodyToDisk(response.body(), EMPLOYEE_FILENAME);
                if (downloadSuccess) {
                    PreferenceUtilities.Preferences.setEmployeeVersion(context, desiredVersion);
                    Log.i(TAG, "Updated employee version");
                }
            }
            @Override public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to download employee file" + desiredVersion);
                Log.e(TAG, throwable.toString());
            }
        });
    }

    /**
     * TODO Update biometrics based off this
     * Check current biometric version.
     * If it does not match desired version, download update from EXML.
     */
    private void updateBiometricsVersion(final String desiredVersion)
    {
        // Check current version
        String myBiometricsVersion = PreferenceUtilities.Preferences.getBiometricsVersion(context);
        if (myBiometricsVersion.equals(desiredVersion)) {
            Log.d(TAG, "Biometrics version up to date.");
            return;
        } else { Log.i(TAG, "Biometrics version NOT up to date. Downloading new version..."); }

        // Download update
        // TODO : Should not be hardcoded
        String readerType = "all";
        String terminalId = "123456";
        Call<ResponseBody> call = atsService.biometricsRequestList(
                readerType, terminalId, desiredVersion, myBiometricsVersion
        );
        System.out.println(call.request().url());
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                boolean downloadSuccess = writeResponseBodyToDisk(response.body(), BIOMETRICS_FILENAME);
                if (downloadSuccess) {
                    PreferenceUtilities.Preferences.setBiometricsVersion(context, desiredVersion);
                    Log.i(TAG, "Updated biometrics version");
                }
            }
            @Override public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to download biometrics file" + desiredVersion);
                Log.e(TAG, throwable.toString());
            }
        });
    }

    private void postBiometricsTest() {

        // Get a template
        AtsService.Biometrics.Templates.Template myTemplate = new AtsService.Biometrics.Templates.Template();
        myTemplate.badge = "111";
        myTemplate.readerType = "all";
        myTemplate.index = "3";
        myTemplate.checksum = "789";
        myTemplate.delete = "false";
        myTemplate.bioTemplate = "eRHeHnjRergefWEGERH";

        // Put template into Templates object
        AtsService.Biometrics.Templates templatesObject = new AtsService.Biometrics.Templates();
        templatesObject.templateList = new ArrayList<>();
        templatesObject.templateList.add(myTemplate);

        // Add templates object to biometrics
        AtsService.Biometrics biometrics = new AtsService.Biometrics();
        biometrics.uid = "1234567890";
        biometrics.templatesList = new ArrayList<>();
        biometrics.templatesList.add(templatesObject);

        Call<ResponseBody> call = atsService.biometricsPost(biometrics);
        call.enqueue (new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response)
            {
                Log.i(TAG, "POST SUCCESS");
                Log.i(TAG, response.message());
            }
            @Override public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                Log.e(TAG, "Failed to post new biometric.");
                Log.e(TAG, throwable.toString());
            }
        });
    }

    private static boolean writeResponseBodyToDisk(ResponseBody body, String fileName)
    {
        try {
            File file = new File(
                    Environment.getExternalStorageDirectory(),
                    fileName
            );

            InputStream inputStream = null;
            OutputStream outputStream = null;

            try
            {
                byte[] fileReader = new byte[4096];
                long fileSizeDownloaded = 0;

                inputStream = body.byteStream();
                outputStream = new FileOutputStream(file);

                while (true) {
                    int read = inputStream.read(fileReader);

                    if (read == -1) {
                        break;
                    }

                    outputStream.write(fileReader, 0, read);

                    fileSizeDownloaded += read;

                    Log.d(TAG, fileName + " file downloaded. Size = " + fileSizeDownloaded + " bytes.");
                }

                outputStream.flush();

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
            finally
            {
                if (inputStream != null) {
                    inputStream.close();
                }
                if (outputStream != null) {
                    outputStream.close();
                }
            }
        } catch (IOException e) {
            return false;
        }
    }

    public String getCurrentVersionFromPrefs(Context context, String key) {
        switch (key) {
            case CONFIG_KEY:
                return PreferenceUtilities.Preferences.getConfigVersion(context);
            case DLD_KEY:
                return PreferenceUtilities.Preferences.getDldVersion(context);
            case FLASH_KEY:
                return PreferenceUtilities.Preferences.getFlashVersion(context);
            default:
                return null;
        }
    }

    public static void saveNewVersionNameToPrefs(final Context context, String key, String desiredVersion) {
        switch (key) {
            case CONFIG_KEY:
                PreferenceUtilities.Preferences.setConfigVersion(context, desiredVersion);
                break;
            case DLD_KEY:
                PreferenceUtilities.Preferences.setDldVersion(context, desiredVersion);
                break;
            case FLASH_KEY:
                PreferenceUtilities.Preferences.setFlashVersion(context, desiredVersion);
                break;
        }

        // TODO : This is for demo purposes only.
        MyDialog.show(context, "New " + key + " Available",
                "The application will now restart.",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        triggerRebirth(context);
                    }
                });
    }

    /**
     * Reboot app.
     */
    public static void triggerRebirth(Context context) {
        PackageManager packageManager = context.getPackageManager();
        Intent intent = packageManager.getLaunchIntentForPackage(context.getPackageName());
        ComponentName componentName = Objects.requireNonNull(intent).getComponent();
        Intent mainIntent = Intent.makeRestartActivityTask(componentName);
        context.startActivity(mainIntent);
        Runtime.getRuntime().exit(0);
    }
}