package com.accutime.app1.services;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.accutime.app1.Application.AtsApplication;
import com.accutime.app1.util.Measurement;
import com.accutime.app1.retrofit.AtsService;
import com.accutime.app1.retrofit.RestAPIs;
import com.accutime.app1.util.PreferenceUtilities;
import com.accutime.app1.util.Util;
import com.accutime.app1.xmlparser.Attribute;
import com.accutime.app1.xmlparser.AttributeList;
import com.accutime.app1.xmlparser.TagNode;
import com.accutime.app1.xmlparser.TreeBuilder;
import com.accutime.app1.xmlparser.TreeNode;
import com.accutime.app1.xmlparser.TreeToXML;
import com.accutime.app1.xmlparser.XmlUtils;

import java.io.StringReader;
import java.util.Iterator;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * JobService to be scheduled by the MyJobScheduler.
 * start another service
 */
@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class GetEmployeesJob extends JobService {

    private static final String TAG = GetEmployeesJob.class.getSimpleName();
    private Measurement getEmployeeMeasurement;

    // TODO : Start in debug mode.
    private boolean DEBUG = false;
    private final boolean ADD_LOCAL_EMPLOYEES = false;

    @Override
    public boolean onStartJob(final JobParameters params) {

        Log.i(TAG, "onStartJob");
        AtsService atsService = RestAPIs.atsService;
        if (atsService == null) return false;

        getEmployeeMeasurement = new Measurement();
        getEmployeeMeasurement.start();

        Call<TagNode> getEmployeesCall = atsService.getEmployeesRaw1();
        getEmployeesCall.enqueue (new Callback<TagNode>() {
            @Override
            public void onResponse(Call<TagNode> call, final Response<TagNode> response) {
                getEmployeeMeasurement.end();
                Log.d(TAG, "Got employee list. Times = " +
                        getEmployeeMeasurement.start + " - " + getEmployeeMeasurement.end +
                        ", Diff = " + getEmployeeMeasurement.diff() +
                        ", Avg =" + getEmployeeMeasurement.avg);

                logAttributes(response);

                // Start a thread to store employee list.
                StoreEmployeesThread storeEmployeesThread = new StoreEmployeesThread(params, response);
                storeEmployeesThread.start();
            }

            @Override
            public void onFailure(Call<TagNode> call, Throwable t) {
                getEmployeeMeasurement.end();
                Log.e(TAG, "Failed getting employee list." +
                        " Finish time = " + getEmployeeMeasurement.end +
                        ", Diff = " + getEmployeeMeasurement.diff() +
                        ", Avg = " + getEmployeeMeasurement.avg +
                        ", Error = ", t);

                PreferenceUtilities.Preferences.setHasEmployees(getApplicationContext(), false);
                jobFinished(params, true);

            }
        });

        // There's still work to be done
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.d(TAG, "onStopJob");
        // Don't reschedule. jobFinished() handles that.
        return false;
    }


    private class StoreEmployeesThread extends Thread {

        private JobParameters params;
        private Response<TagNode> mResponse;
        private Measurement storeEmployeeMeasurement;
        private AtsApplication atsApplication;
        private XmlUtils.ValueTuple rootNode = null;

        StoreEmployeesThread(JobParameters params, Response<TagNode> response) {
            this.mResponse = response;
            this.params = params;
        }

        @Override
        public void run() {

            atsApplication = (AtsApplication) getApplicationContext();

            storeEmployeeMeasurement = new Measurement();
            storeEmployeeMeasurement.start();

            try {

                // We're in debug mode.
                if (DEBUG) {

                    TreeBuilder builder = new TreeBuilder();
                    StringReader stringReader = new StringReader(getDebugEmployees());
                    TreeNode localEmployeesNode = builder.parseXML(stringReader);

                    if (localEmployeesNode instanceof TagNode) {
                        Log.d(TAG, "STORING DEBUG EMPLOYEE");
                        rootNode = storeEmployees((TagNode) localEmployeesNode);
                    }
                }

                else {
                    Log.d(TAG, "STORING EMPLOYEES FROM SERVER");
                    rootNode = storeEmployees(mResponse.body());
                }


                // Debug
                if (ADD_LOCAL_EMPLOYEES) {

                    String[] localEmployees = getLocalEmployees();

                    // Store local employees.
                    // TODO : Why is this starting at 1? That skips jagat.
                    for (int i = 1; i <= localEmployees.length; i++) { // TODO: Was <=
                        StringReader thisEmployee = new StringReader(localEmployees[i]);
                        TreeNode localEmp = new TreeBuilder().parseXML(thisEmployee);

                        if (localEmp instanceof TagNode) {
                            XmlUtils.ValueTuple newEmp = atsApplication.storeObject(
                                    (TagNode) localEmp,
                                    "/Employees"
                            );

                            // This gives null pointer? But not always. At least not the first time.
                            XmlUtils.ValueTuple parent = XmlUtils.addChild(
                                    rootNode,
                                    newEmp,
                                    atsApplication.getDbRelations()
                            );
                        }
                    }

                    // Print all keys.
                    String keyslog = XmlUtils.printAllKeys(
                            atsApplication.getDbRelations(),
                            atsApplication.getDbIdToPath()
                    );
                    Log.v(TAG, "Keys Log: \n" + keyslog);

                    logFullTree();
                }

                AtsApplication.badgeNodes = atsApplication.getKeyValuePairsForNode(
                        "/Employees/Employee/badge"
                );

                Log.v(TAG, "Badge Nodes: " + AtsApplication.badgeNodes.toString());
                for (String key : AtsApplication.badgeNodes.keySet()) {
                    Log.v(TAG, "Key: " + key);
                }

                // Success
                PreferenceUtilities.Preferences.setHasEmployees(getApplicationContext(), true);

                logFullTree();

                // Print all keys.
//                if (DEBUG) {
//                    String str = XmlUtils.printAllKeys(
//                            atsApplication.getDbRelations(),
//                            atsApplication.getDbIdToPath()
//                    );
//                    Log.v(TAG, "Print All Keys:\n" + str);

//                    Log.v(TAG, "Badge? " + str.substring(str.indexOf("badge")));
//                }

                jobFinished(params, false);

            } catch (Exception e) {
                e.printStackTrace();
                storeEmployeeMeasurement.end();
                Log.d(TAG, "Got exception while storing employee list. Finish time = " + storeEmployeeMeasurement.end +
                        ", Diff = " + storeEmployeeMeasurement.diff() +
                        ", Avg = " + storeEmployeeMeasurement.avg);
                jobFinished(params, true);
            }
        }

        /**
         * Store employee list and record time.
         */
        private XmlUtils.ValueTuple storeEmployees(TagNode node) throws Exception {

            rootNode = atsApplication.storeObject(node, "");
            rootNode.commitRelation(atsApplication.getDbRelations());
            atsApplication.storeRootNodeName(rootNode);

            storeEmployeeMeasurement.end();
            Log.d(TAG, "Stored employee list. Time taken : " + storeEmployeeMeasurement.show());

            logFullTree();

            return rootNode;
        }

        private void logFullTree() throws Exception {
            TreeNode tnode = XmlUtils.retrieveObject(
                    rootNode.nodeId,
                    atsApplication.getDbIdToPath(),
                    atsApplication.getDbPathToId(),
                    atsApplication.getDbRelations()
            );

            if (tnode != null) {
                TreeToXML txml = new TreeToXML(tnode);
                Util.Log.d(TAG, "Back to xml: " + txml);
            }
        }
    }

    /**
     * For debugging.
     * Expected response from server.
     */
    private String getDebugEmployees() {
        return "<Employees>" +
                "  <Employee badge=\"900000212\" supervisor_level=\"1\" " +
                "        schedule_id=\"3479\" break_schedule_id=\"3479\"" +
                "        meal_schedule_id=\"3479\" language=\"\"" +
                "        >\n" +

                "    <Name>William Bailey</Name>\n" +
                "    <WeekToDateHours>0</WeekToDateHours>\n" +
                "    <SecondPunchTimeRestrict>1</SecondPunchTimeRestrict>\n" +

                "  </Employee>\n" +
                "</Employees>";
    }

    /**
     * For debugging.
     * Collection of test employees.
     */
    private String[] getLocalEmployees() {

        String localEmployee1 =
                "<Employee badge=\"33445566\" supervisor_level=\"2\"" +
                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
                        "meal_schedule_id=\"12123\" language=\"english\"" +
                        " > " +
                        "<Name>Jagat Brahma</Name> " +
                        "<WeekToDateHours>0</WeekToDateHours> "+
                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
                        "</Employee> " ;
        String localEmployee2 =
                "<Employee badge=\"22334455\" supervisor_level=\"9\"" +
                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
                        "meal_schedule_id=\"12123\" language=\"english\"" +
                        " > " +
                        "<Name>Carlos Bernal</Name> " +
                        "<WeekToDateHours>0</WeekToDateHours> "+
                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
                        "</Employee> " ;
        String localEmployee3 =
                "<Employee badge=\"11223344\" supervisor_level=\"9\"" +
                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
                        "meal_schedule_id=\"12123\" language=\"english\"" +
                        " > " +
                        "<Name>Jean-Pierre Van de Capelle</Name> " +
                        "<WeekToDateHours>0</WeekToDateHours> "+
                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
                        "</Employee> " ;
        String localEmployee4 =
                "<Employee badge=\"44556677\" supervisor_level=\"3\"" +
                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
                        "meal_schedule_id=\"12123\" language=\"english\"" +
                        " > " +
                        "<Name>Asha Hemingway</Name> " +
                        "<WeekToDateHours>0</WeekToDateHours> "+
                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
                        "</Employee> " ;
        String localEmployee5 =
                "<Employee badge=\"55667788\" supervisor_level=\"4\"" +
                        " schedule_id=\"34012\" break_schedule_id=\"11981\" " +
                        "meal_schedule_id=\"12123\" language=\"english\"" +
                        " > " +
                        "<Name>Ryan McColgan</Name> " +
                        "<WeekToDateHours>0</WeekToDateHours> "+
                        "<SecondPunchTimeRestrict>1</SecondPunchTimeRestrict> "+
                        "</Employee> " ;

        return new String[]{
                localEmployee1,
                localEmployee2,
                localEmployee3,
                localEmployee4,
                localEmployee5
        };
    }

    /**
     * For debugging.
     * Log the attributes from the mResponse TagNode.
     */
    private void logAttributes(Response<TagNode> response) {
        TagNode tagNode = response.body();
        AttributeList attrList = tagNode.getAttrList();
        if (attrList != null && attrList.hasAttributes()) {
            Iterator iter = attrList.getIterator();
            Attribute attr;

            int index = 0;
            do {
                attr = (Attribute) iter.next();
                Log.d(TAG,
                        "Attribute from TagNode[" + index + "]: " + attr +
                                ", Value: " +  attr.getValue());
                index++;
            } while (iter.hasNext());
        }

        Log.d(TAG, "Full: " + tagNode.findAttribute("full").getValue());
        Log.d(TAG, "Version: " + tagNode.findAttribute("version").getValue());
    }
}