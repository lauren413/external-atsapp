package com.accutime.app1.common;

import android.os.Build;
import android.util.Log;

import com.accutime.app1.util.Util;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by jpbrahma on 9/7/16.
 * These say constants but they are not final.
 * They are set based off of constants file.
 */
public class Constants {
    public static String TAG = Constants.class.getSimpleName();

    private static JSONObject preferencesObject;
    private static final String PREFERENCES_KEY = "preferences";
    private static final String DISPLAY_RESULTS_TIMER_KEY = "displayresultstimer";
    private static final String STATE_TIMEOUT_KEY = "statetimeout";

    private static JSONObject hostObject;
    private static final String HOST_KEY = "host";
    private static final String HOST_URL_KEY = "hosturl";

    private static JSONObject localeObject;
    private static final String LOCALE_KEY = "preferences";
    private static final String LANGUAGE_KEY = "language";
    private static final String TIMEZONE_KEY = "timezone";

    private static JSONObject biometricObject;
    private static final String BIOMETRIC_KEY = "biometric";

    private static JSONObject deviceObject;
    private static final String DEVICE_KEY = "device";

    public static void setConstants(JSONObject configJson)
    {
        preferencesObject = Util.getJsonObjectFrom(configJson, PREFERENCES_KEY);
        hostObject = Util.getJsonObjectFrom(configJson, HOST_KEY);
        localeObject = Util.getJsonObjectFrom(configJson, LOCALE_KEY);
        biometricObject = Util.getJsonObjectFrom(configJson, BIOMETRIC_KEY);
        deviceObject = Util.getJsonObjectFrom(configJson, DEVICE_KEY);

        // preferences
        Integer newStateTimeout = getFromPreferences(STATE_TIMEOUT_KEY);
        Integer newDialogDelay = getFromPreferences(DISPLAY_RESULTS_TIMER_KEY);
        if (Util.objectExists(newStateTimeout)) STATE_TIME = newStateTimeout;
        if (Util.objectExists(newDialogDelay)) DIALOG_DELAY = newDialogDelay;

        // host
        String newHostURL = getFromHost(HOST_URL_KEY);
        if (Util.objectExists(newHostURL)) HOST_URL = newHostURL;

        // locale
        String newLanguage = getFromLocale(LANGUAGE_KEY);
        if (Util.objectExists(newLanguage)) LANGUAGE = newLanguage;

        String newTimeZone = getFromHost(TIMEZONE_KEY);
        if (Util.objectExists(newTimeZone)) TIMEZONE = newTimeZone;
    }

    private static Integer getFromPreferences(String key) {
        return Util.getIntegerFromJson(preferencesObject, key);
    }

    private static String getFromHost(String key) {
        return Util.getStringFromJson(hostObject, key);
    }

    private static String getFromLocale(String key) {
        return Util.getStringFromJson(localeObject, key);
    }

    /**
     * TODO : Do I need this?
     * I definitely need the local one.
     */
    public static String getDeviceIpAddress() {
        Log.d(TAG, "getDeviceIpAddress");

        if (Build.FINGERPRINT.contains("generic")){
            Log.d(TAG,"\tReturning deviceIpAddress_remote");
            return deviceIpAddress_remote;
        } else {
            Log.d(TAG,"\tReturning deviceIpAddress_local");
            return deviceIpAddress_local;
        }
    }



//    public static final String engineXmlIpAddress = "http://jfqa-exml.accu-time.com/";
//    public static final String deviceIpAddress = "http://192.168.15.97:8000";
//    public static final String engineXmlIpAddress = "http://bmqa-exml.accu-time.com";
    public static final String deviceIpAddress_remote = "http://192.168.130.150";
//    public static final String deviceIpAddress_remote = "http://192.168.130.102";
    public static final String deviceIpAddress_local = "http://localhost";
//    public static final String deviceIpAddress = "http://192.168.130.128:8000";

    ///// KeyBoard Event processor  /////////
    public static HashMap<Integer, String> scanCodeToKeyVal = new HashMap<>();
    static {
        scanCodeToKeyVal.put(30, "1");
        scanCodeToKeyVal.put(31, "2");
        scanCodeToKeyVal.put(32, "3");
        scanCodeToKeyVal.put(33, "4");
        scanCodeToKeyVal.put(34, "5");
        scanCodeToKeyVal.put(35, "6");
        scanCodeToKeyVal.put(36, "7");
        scanCodeToKeyVal.put(37, "8");
        scanCodeToKeyVal.put(38, "9");
        scanCodeToKeyVal.put(39, "0");
        scanCodeToKeyVal.put(42, "c");
        scanCodeToKeyVal.put(40, "e");
        scanCodeToKeyVal.put(58, "f1");
        scanCodeToKeyVal.put(59, "f2");
        scanCodeToKeyVal.put(60, "f3");
        scanCodeToKeyVal.put(61, "f4");
        scanCodeToKeyVal.put(62, "f5");
        scanCodeToKeyVal.put(63, "f6");
        scanCodeToKeyVal.put(64, "f7");
        scanCodeToKeyVal.put(65, "f8");
    }

    public static final String BUTTON_1_KEY = "Btn1";
    public static final String BUTTON_2_KEY = "Btn2";
    public static final String BUTTON_3_KEY = "Btn3";
    public static final String BUTTON_4_KEY = "Btn4";
    public static final String BUTTON_5_KEY = "Btn5";
    public static final String BUTTON_6_KEY = "Btn6";
    public static final String BUTTON_7_KEY = "Btn7";
    public static final String BUTTON_8_KEY = "Btn8";

    public static final String TEXT_FIELD_KEY = "TextField";

    public static final String KEY_STATE_PATH = "state_path";
    public static final String EMPLOYEE_EXTRA = "employee";
    public static final String KEY_BADGE = "badge";
//    public static final String KEY_ACTION = "action";
    public static final String KEY_MESSAGE = "message";

    public static final String STATE_EXTRA = "State";

    public static final String ACTION_IN = "IN";
    public static final String ACTION_OUT = "OUT";
    public static final String ACTION_MEAL = "MEAL";
    public static final String ACTION_BREAK = "BREAK";

    public static final String ACTION_LOGIN = "Login";
    public static final String ACTION_ADD_FINGERPRINT = "AddFingerprint";

    public static final String KEY_MAIN_STATE = "State_Main";
    public static final String KEY_LAYOUT = "Layout";
    public static final String KEY_LABEL = "Label";
    public static final String KEY_ICON = "Icon";
    public static final String KEY_ACTION = "Action";
    public static final String KEY_SWITCH_STATE = "SwitchState";
    public static final String KEY_NEXTSTATE = "NextState";
    public static final String KEY_POST_ACTION_MESSAGE = "PostActionMessage";
    public static final String KEY_SUCCESS = "Success";
    public static final String KEY_FAILURE = "Failure";
    public static final String KEY_HINT = "Hint";
    public static final String KEY_LOGIN = "Login";

    public static final String LAYOUT_MENU_MAIN = "menu_main";
    public static final String LAYOUT_MENU_CENTER_LEFT_RIGHT = "menu_center_left_right";
    public static final String LAYOUT_MENU_STRETCH_LEFT_RIGHT = "menu_stretch_left_right";
    public static final String LAYOUT_MENU_STRETCH_TOP_BOTTOM = "menu_stretch_top_bottom";

    public static final String ICON_PUNCH_IN = "icon_punch_in";
    public static final String ICON_PUNCH_OUT = "icon_punch_out";
    public static final String ICON_MANAGER = "icon_manager";
    public static final String ICON_ADD_FINGERPRINT = "icon_add_fingerprint";
    public static final String ICON_BACK = "icon_back";

    public static final String CONFIG_FILENAME = "ConfigFile";
    public static final String PROFILE_FILENAME = "DldFile";
    public static final String FLASH_FILENAME = "FlashFile";

    public static String HOST_URL = "https://atsqa-wd.accu-time.com";
    public static int STATE_TIME = 10000;
    public static int DIALOG_DELAY = 5000;

    public static String SERIAL_NUMBER = "0000000";
    public static String TIMEZONE = "America/New_York";
    public static String LANGUAGE = "English";
}
