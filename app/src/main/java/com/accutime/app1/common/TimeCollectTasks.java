package com.accutime.app1.common;

import android.content.Context;

/**
 * Created by mkemp on 5/15/18.
 */

public class TimeCollectTasks {

    public static final String TAG = TimeCollectTasks.class.getSimpleName();

    public static final String ACTION_PUNCH_IN = "punch-in";
    public static final String ACTION_DOWNLOAD_EMPLOYEES = "download-employees";

    public static void executeTask(Context context, String action) {
        if (ACTION_PUNCH_IN.equals(action)) {
            sendPunchInRequest(context);
        } else if (ACTION_DOWNLOAD_EMPLOYEES.equals(action)) {
            downloadEmployeesFromEXML(context);
        }
    }

    private static void sendPunchInRequest(Context context) {

    }

    private static void downloadEmployeesFromEXML(Context context) {

    }
}
