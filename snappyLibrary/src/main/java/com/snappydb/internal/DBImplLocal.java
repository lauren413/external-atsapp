package com.snappydb.internal;

import android.text.TextUtils;
import android.util.Log;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.snappydb.DBLocal;
import com.snappydb.KeyIterator;
import com.snappydb.SnappydbException;

import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 * Created by jpbrahma on 3/19/15.
 */
public class DBImplLocal implements DBLocal {
    public static final String TAG=DBImplLocal.class.getSimpleName();
    private static final String LIB_NAME = "snappydb_native";
    private static final int LIMIT_MAX = Integer.MAX_VALUE - 8;

    private String dbPath;
    private Kryo kryo;

    static {
        System.loadLibrary(LIB_NAME);
    }

    public DBImplLocal(String path, Kryo... kryo) throws SnappydbException {
        this.dbPath = path;

        if (null != kryo && kryo.length > 0) {
            this.kryo = kryo[0];
        } else {
            this.kryo = new Kryo();
            this.kryo.setAsmEnabled(true);
        }
        __open(dbPath);
    }

    // ***********************
    // *     DB MANAGEMENT
    // ***********************

    @Override
    public void close() {
        __close();
    }

    @Override
    public void destroy() throws SnappydbException {
        __destroy(dbPath);
    }

    @Override
    public boolean isOpen() throws SnappydbException {
        return __isOpen();
    }

    String appendTimestamp(String key, long timeStamp) {
        StringBuilder ss = new StringBuilder(key);
        ss.append("!");
        ss.append(timeStamp);
        return ss.toString();
    }

    String appendTimestampString(String key, String timeStamp) {
        StringBuilder ss = new StringBuilder(key);
        ss.append("!");
        ss.append(timeStamp);
        return ss.toString();
    }

    String appendTimestampString(String key) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSZ", Locale.US);
        return appendTimestampString(key, sdf.format(new Date()));
    }

    String appendTimestamp(String key) {
        return appendTimestamp(key, Calendar.getInstance().getTimeInMillis());
    }

    ArrayList<String> findRelevantKeys(String key) throws SnappydbException {
        String[] keys = findKeys(key);
        ArrayList<String> validKeys = new ArrayList<String>();
        for( String k :keys)  {
            String[] ss = k.split("!");
            if (ss[0].compareTo(key) == 0) {
                validKeys.add(k);
            }
        }
        return validKeys;
    }


    // ***********************
    // *       CREATE
    // ***********************
    @Override
    public void put(String key, String value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, String value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }
        __put(key, value);
    }



    @Override
    public void put(String key, Serializable value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, Serializable value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        kryo.register(value.getClass());

        Output output = new Output(stream);
        try {
            kryo.writeObject(output, value);
            output.close();

            __put(key, stream.toByteArray());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException(e.getMessage());
        }
    }


    @Override
    public void put(String key, Object value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, Object value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);

        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        kryo.register(value.getClass());

        Output output = new Output(stream);
        try {
            kryo.writeObject(output, value);
            output.close();

            __put(key, stream.toByteArray());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException(e.getMessage());
        }
    }

    @Override
    public void put(String key, byte[] value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, byte[] value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }
        __put(key, value);
    }


    @Override
    public void put(String key, Serializable[] value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, Serializable[] value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        kryo.register(value.getClass());

        Output output = new Output(stream);
        try {
            kryo.writeObject(output, value);
            output.close();

            __put(key, stream.toByteArray());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Kryo exception " + e.getMessage());
        }
    }

    @Override
    public void put(String key, Object[] value) throws SnappydbException {
        put(key, value, false);
    }

    @Override
    public void put(String key, Object[] value, boolean addTimestamp) throws SnappydbException {
        checkArgs(key, value);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        kryo.register(value.getClass());

        Output output = new Output(stream);
        try {
            kryo.writeObject(output, value);
            output.close();

            __put(key, stream.toByteArray());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Kryo exception " + e.getMessage());
        }
    }


    @Override
    public void putShort(String key, short val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putShort(String key, short val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);

        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        __putShort(key, val);
    }


    @Override
    public void putInt(String key, int val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putInt(String key, int val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        __putInt(key, val);
    }



    @Override
    public void putBoolean(String key, boolean val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putBoolean(String key, boolean val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }
        __putBoolean(key, val);
    }


    @Override
    public void putDouble(String key, double val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putDouble(String key, double val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);

        if (addTimestamp) {
            key = appendTimestamp(key);
        }
        __putDouble(key, val);
    }



    @Override
    public void putFloat(String key, float val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putFloat(String key, float val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        __putFloat(key, val);
    }


    @Override
    public void putLong(String key, long val) throws SnappydbException {
        put(key, val, false);
    }

    @Override
    public void putLong(String key, long val, boolean addTimestamp) throws SnappydbException {
        checkKey(key);
        if (addTimestamp) {
            key = appendTimestamp(key);
        }

        __putLong(key, val);
    }

    // ***********************
    // *      DELETE
    // ***********************

    @Override
    public void del(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        for(String kee: validKeys) {
            __del(kee);
        }
    }

    @Override
    public void delAt(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        String lee = appendTimestamp(key,timeStamp);
        __del(lee);
    }

    @Override
    public void delBefore(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        String lee = appendTimestamp(key,timeStamp);
        for(String kee: validKeys) {
            if (lee.compareTo(kee) <= 0) {
                __del(kee);
            }
        }
    }

    @Override
    public void delAfter(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        String lee = appendTimestamp(key,timeStamp);
        for(String kee: validKeys) {
            if (lee.compareTo(kee) >= 0) {
                __del(kee);
            }
        }
    }

    @Override
    public void delBetween(String key, long timeStampStart, long timeStampEnd) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        String leeStart = appendTimestamp(key,timeStampStart);
        String leeEnd = appendTimestamp(key,timeStampEnd);
        for(String kee: validKeys) {
            if (kee.compareTo(leeStart) >= 0 && kee.compareTo(leeEnd) <= 0) {
                __del(kee);
            }
        }
    }

    // ***********************
    // *       RETRIEVE
    // ***********************

    @Override
    public <T extends Serializable> T get(String key, long timeStamp, Class<T> className) throws SnappydbException {
        checkArgs(key, className);

        if (className.isArray()) {
            throw new SnappydbException(
                    "You should call getArray instead");
        }

        byte[] data = getBytes(key, timeStamp);

        kryo.register(className);

        Input input = new Input(data);
        try {
            return kryo.readObject(input, className);

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                    "please use getArray instead " + e.getMessage());
        } finally {
            input.close();
        }
    }

    @Override
    public <T extends Serializable> T[] get(String key, Class<T> className)
            throws SnappydbException {
        checkArgs(key, className);

        if (className.isArray()) {
            throw new SnappydbException(
                    "You should call getArray instead");
        }

        byte[][] data = getBytes(key);
        kryo.register(className);

        Input[] inputs = new Input[data.length];
        T[] ts = (T[]) Array.newInstance(className, data.length);
        for(int ii=0; ii<data.length; ii++) {
            inputs[ii] = new Input(data[ii]);
            try {
                ts[ii] = kryo.readObject(inputs[ii], className);
            } catch (Exception e) {
                e.printStackTrace();
                throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                        "please use getArray instead " + e.getMessage());
            } finally {
                inputs[ii].close();
            }
        }
        return ts;
    }


    @Override
    public <T> T getObject(String key, long timeStamp, Class<T> className) throws SnappydbException {
                checkArgs(key, className);
        checkArgs(key, className);
        if (className.isArray()) {
            throw new SnappydbException(
                    "You should call getObjectArray instead");
        }

        byte[] data = getBytes(key, timeStamp);

        kryo.register(className);

        Input input = new Input(data);
        try {
            return kryo.readObject(input, className);

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                    "please use getObjectArray instead " + e.getMessage());
        } finally {
            input.close();
        }
    }

    @Override
    public <T> T[] getObject (String key, Class<T> className) throws SnappydbException {
        checkArgs(key, className);

        if (className.isArray()) {
            throw new SnappydbException(
                    "You should call getObjectArray instead");
        }

        byte[][] data = getBytes(key);

        kryo.register(className);

        Input[] inputs = new Input[data.length];
        T[] ts = (T[]) Array.newInstance(className, data.length);

        for (int ii = 0; ii < data.length; ii++) {
            inputs[ii] = new Input(data[ii]);
            try {
                ts[ii] = kryo.readObject(inputs[ii], className);
            } catch (Exception e) {
                e.printStackTrace();
                throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                        "please use getObjectArray instead " + e.getMessage());
            } finally {
                inputs[ii].close();
            }
        }
        return ts;
    }


    public <T extends Serializable> T[][] getArray (String key, Class<T> className)
            throws SnappydbException {
        checkArgs(key, className);

        byte[][] data = getBytes(key);
        int[] sizes = new int[data.length];
        for (int bb=0; bb<data.length; bb++)
            sizes[bb] = data[bb].length;

        kryo.register(className);

        Input[] inputs = new Input[data.length];
        T[][] ts = (T[][]) Array.newInstance(className, sizes);

        for(int ii=0; ii<data.length; ii++) {
            inputs[ii] = new Input(data[ii]);
            T[] array = (T[]) Array.newInstance(className, 0);
            try {
                ts[ii] =  (T[]) kryo.readObject(inputs[ii], array.getClass());
            } catch (Exception e) {
                e.printStackTrace();
                throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                        "please use getArray instead " + e.getMessage());
            } finally {
                inputs[ii].close();
            }
        }
        return ts;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T extends Serializable> T[] getArray(String key, long timeStamp, Class<T> className)
            throws SnappydbException {
        checkArgs(key, className);
        key = appendTimestamp(key, timeStamp);

        byte[] data = __getBytes(key);

        kryo.register(className);

        Input input = new Input(data);
        T[] array = (T[]) Array.newInstance(className, 0);

        try {
            return (T[]) kryo.readObject(input, array.getClass());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Maybe you tried to retrieve an array using this method " +
                    "? please use getArray instead " + e.getMessage());
        } finally {
            input.close();
        }
    }


    @SuppressWarnings("unchecked")
    @Override
    public <T> T[][] getObjectArray(String key, Class<T> className) throws SnappydbException {
        checkArgs(key, className);

        byte[][] data = getBytes(key);
        int[] sizes = new int[data.length];
        for (int bb=0; bb<data.length; bb++) {
            sizes[bb] = data[bb].length;
        }

        kryo.register(className);

        Input[] inputs = new Input[data.length];
        T[][] ts = (T[][]) Array.newInstance(className, sizes);

        for(int ii=0; ii<data.length; ii++) {
            inputs[ii] = new Input(data[ii]);
            T[] array = (T[]) Array.newInstance(className, 0);
            try {
                ts[ii] =  (T[]) kryo.readObject(inputs[ii], array.getClass());
            } catch (Exception e) {
                e.printStackTrace();
                throw new SnappydbException("Maybe you tried to retrieve an array using this method ? " +
                        "please use getArray instead " + e.getMessage());
            } finally {
                inputs[ii].close();
            }
        }
        return ts;
    }

    @SuppressWarnings("unchecked")
    @Override
    public <T> T[] getObjectArray(String key, long timeStamp, Class<T> className) throws SnappydbException {
        checkArgs(key, className);
        key = appendTimestamp(key, timeStamp);

        byte[] data = __getBytes(key);

        kryo.register(className);

        Input input = new Input(data);
        T[] array = (T[]) Array.newInstance(className, 0);

        try {
            return (T[]) kryo.readObject(input, array.getClass());

        } catch (Exception e) {
            e.printStackTrace();
            throw new SnappydbException("Maybe you tried to retrieve an array using this method " +
                    "? please use getArray instead " + e.getMessage());
        } finally {
            input.close();
        }
    }

    @Override
    public byte[][] getBytes(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        byte[][] values = new byte[validKeys.size()][];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getBytes(validKeys.get(ii));

        return values;
    }

    @Override
    public byte[] getBytes(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key = appendTimestamp(key, timeStamp);

        return __getBytes(key);
    }

    @Override
    public String[] get(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        String[] values = new String[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++) {
            values[ii] = __get(validKeys.get(ii));
        }
        return values;
    }

    @Override
    public String get(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key = appendTimestamp(key,timeStamp);
        return __get(key);
    }

    @Override
    public short[] getShort(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        short[] values = new short[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getShort(validKeys.get(ii));

        return values;
    }

    @Override
    public short getShort(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key,timeStamp);

        return __getShort(key);
    }


    @Override
    public int[] getInt(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        int[] values = new int[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getInt(validKeys.get(ii));

        return values;
    }

    @Override
    public int getInt(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key,timeStamp);

        return __getInt(key);
    }


    @Override
    public boolean[] getBoolean(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        boolean[] values = new boolean[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getBoolean(validKeys.get(ii));

        return values;
    }

    @Override
    public boolean getBoolean(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key,timeStamp);

        return __getBoolean(key);
    }


    @Override
    public double[] getDouble(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        double[] values = new double[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getDouble(validKeys.get(ii));

        return values;
    }

    @Override
    public double getDouble(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key, timeStamp);

        return __getDouble(key);
    }


    @Override
    public long[] getLong(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        long[] values = new long[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getLong(validKeys.get(ii));

        return values;
    }

    @Override
    public long getLong(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key, timeStamp);

        return __getLong(key);
    }


    @Override
    public float[] getFloat(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        float[] values = new float[validKeys.size()];
        for(int ii=0; ii<validKeys.size(); ii++)
            values[ii] = __getFloat(validKeys.get(ii));

        return values;
    }

    @Override
    public float getFloat(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key, timeStamp);

        return __getFloat(key);
    }

    //****************************
    //*      KEYS OPERATIONS
    //****************************
    @Override
    public boolean exists(String key, long timeStamp) throws SnappydbException {
        checkKey(key);
        key=appendTimestamp(key, timeStamp);
        return __exists(key);
    }

    @Override
    public boolean exists(String key) throws SnappydbException {
        checkKey(key);
        ArrayList<String> validKeys = findRelevantKeys(key);
        if (validKeys.size() > 0)
            return true;
        return false;
    }

    @Override
    public String[] findKeys(String prefix) throws SnappydbException {
        return findKeys(prefix, 0, LIMIT_MAX);
    }

    @Override
    public String[] findKeys(String prefix, int offset) throws SnappydbException {
        return findKeys(prefix, offset, LIMIT_MAX);
    }

    @Override
    public String[] findKeys(String prefix, int offset, int limit) throws SnappydbException {
        checkPrefix(prefix);
        checkOffsetLimit(offset, limit);

        return __findKeys(prefix, offset, limit);
    }

    @Override
    public int countKeys(String prefix) throws SnappydbException {
        checkPrefix(prefix);

        return __countKeys(prefix);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix)
            throws SnappydbException {
        return findKeysBetween(startPrefix, endPrefix, 0, LIMIT_MAX);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix, int offset)
            throws SnappydbException {
        return findKeysBetween(startPrefix, endPrefix, offset, LIMIT_MAX);
    }

    @Override
    public String[] findKeysBetween(String startPrefix, String endPrefix, int offset, int limit)
            throws SnappydbException {
        checkRange(startPrefix, endPrefix);
        checkOffsetLimit(offset, limit);

        return __findKeysBetween(startPrefix, endPrefix, offset, limit);
    }

    @Override
    public int countKeysBetween(String startPrefix, String endPrefix)
            throws SnappydbException {
        checkRange(startPrefix, endPrefix);

        return __countKeysBetween(startPrefix, endPrefix);
    }

    //***********************
    //*      ITERATORS
    //***********************
    @Override
    public KeyIterator allKeysIterator()
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(null, false), null, false);
    }

    @Override
    public KeyIterator allKeysReverseIterator()
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(null, true),  null, true);
    }

    @Override
    public KeyIterator findKeysIterator(String prefix)
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(prefix, false), null, false);
    }

    @Override
    public KeyIterator findKeysReverseIterator(String prefix)
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(prefix, true), null, true);
    }

    @Override
    public KeyIterator findKeysBetweenIterator(String startPrefix, String endPrefix)
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(startPrefix, false), endPrefix, false);
    }

    @Override
    public KeyIterator findKeysBetweenReverseIterator(String startPrefix, String endPrefix)
            throws SnappydbException {
        return new KeyIteratorImplLocal(this, __findKeysIterator(startPrefix, true), endPrefix, true);
    }

    //*********************************
    //*      KRYO SERIALIZATION
    //*********************************
    @Override
    public Kryo getKryoInstance() {
        return this.kryo;
    }

    // ***********************
    // *      UTILS
    // ***********************

    private void checkArgs (String key, Object value) throws SnappydbException {
        checkArgNotEmpty (key, "Key must not be empty");

        if (null == value) {
            throw new SnappydbException("Value must not be empty");
        }
    }

    private void checkPrefix (String prefix) throws SnappydbException {
        checkArgNotEmpty (prefix, "Starting prefix must not be empty");
    }

    private void checkRange (String startPrefix, String endPrefix) throws SnappydbException {
        checkArgNotEmpty (startPrefix, "Starting prefix must not be empty");
        checkArgNotEmpty (startPrefix, "Ending prefix must not be empty");
    }

    private void checkKey (String key) throws SnappydbException {
        checkArgNotEmpty (key, "Key must not be empty");
    }

    private void checkArgNotEmpty (String arg, String errorMsg) throws SnappydbException {
        if (TextUtils.isEmpty(arg)) {
            throw new SnappydbException(errorMsg);
        }
    }

    private void checkOffsetLimit (int offset, int limit) throws SnappydbException {
        if (offset < 0) {
            throw new SnappydbException("Offset must not be negative");
        }
        if (limit <= 0) {
            throw new SnappydbException("Limit must not be 0 or negative");
        }
    }


    // native code
    private native void __close();

    private native void __open(String dbName) throws SnappydbException;

    private native void __destroy(String dbName) throws SnappydbException;

    private native boolean __isOpen() throws SnappydbException;

    private native void __put(String key, byte[] value) throws SnappydbException;

    private native void __put(String key, String value) throws SnappydbException;

    private native void __putShort(String key, short val) throws SnappydbException;

    private native void __putInt(String key, int val) throws SnappydbException;

    private native void __putBoolean(String key, boolean val) throws SnappydbException;

    private native void __putDouble(String key, double val) throws SnappydbException;

    private native void __putFloat(String key, float val) throws SnappydbException;

    private native void __putLong(String key, long val) throws SnappydbException;

    private native void __del(String key) throws SnappydbException;

    private native byte[] __getBytes(String key) throws SnappydbException;

    private native String __get(String key) throws SnappydbException;

    private native short __getShort(String key) throws SnappydbException;

    private native int __getInt(String key) throws SnappydbException;

    private native boolean __getBoolean(String key) throws SnappydbException;

    private native double __getDouble(String key) throws SnappydbException;

    private native long __getLong(String key) throws SnappydbException;

    private native float __getFloat(String key) throws SnappydbException;

    private native boolean __exists(String key) throws SnappydbException;

    private native String[] __findKeys (String prefix, int offset, int limit) throws SnappydbException;

    private native int __countKeys (String prefix) throws SnappydbException;

    private native String[] __findKeysBetween(String startPrefix, String endPrefix, int offset, int limit) throws SnappydbException;

    private native int __countKeysBetween(String startPrefix, String endPrefix) throws SnappydbException;

    native long __findKeysIterator(String prefix, boolean reverse) throws SnappydbException;

    native String[] __iteratorNextArray(long ptr, String endPrefix, boolean reverse, int max) throws SnappydbException;

    native boolean __iteratorIsValid(long ptr, String endPrefix, boolean reverse);

    native void __iteratorClose(long ptr);
}
