LOCAL_PATH := $(call my-dir)
#include $(LOCAL_PATH)/src/main/jni/Android.mk

include $(CLEAR_VARS)
LOCAL_MODULE := SnappyLibrary
LOCAL_MODULE_TAGS := eng
LOCAL_MANIFEST_FILE := src/main/AndroidManifest.xml
LOCAL_SRC_FILES := $(call all-java-files-under, src/main/java) 
LOCAL_AAPT_FLAGS += --auto-add-overlay
LOCAL_STATIC_JAVA_LIBRARIES := kryo kryo-serializers
#LOCAL_JNI_SHARED_LIBRARIES := snappydb_native
LOCAL_SDK_VERSION := 17
include $(BUILD_JAVA_LIBRARY)

include $(CLEAR_VARS)
LOCAL_PREBUILT_STATIC_JAVA_LIBRARIES := kryo:libs/kryo-2.24.0.jar kryo-serializers:libs/kryo-serializers-0.26.jar
include $(BUILD_MULTI_PREBUILT)

